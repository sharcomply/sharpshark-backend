FROM node:16

ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.9.0/wait /wait
RUN chmod +x /wait

ADD docker.sh /docker.sh
RUN chmod +x /docker.sh

WORKDIR /usr/src/app
COPY package.json ./
RUN npm install
COPY . .
EXPOSE 3000

CMD /wait && /docker.sh