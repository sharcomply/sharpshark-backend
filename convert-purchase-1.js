require('dotenv').config();
require('./server/configs/mongoose');
const Purchase = require('mongoose').models.Purchase;

(async() => {
  const purchases = await Purchase.find({});
  for (let i = 0; i < purchases.length; i++) {
    purchases[i].amountFloat = Number.parseFloat(purchases[i].amount.split(' ')[0]);
    await purchases[i].save();
  }
})();