const Promise = require("bluebird");
require('dotenv').config();
const generateAndUploadQr = require("./server/utils/generate-and-upload-qr");

(async (network, transactionHash) => {
  const ownerId = "6433fed0ea07d980742503a2";
  const tokenId = "2456745697499186027261742662668860016226684158437926445806";
  const metadataUrl = "https://ipfs.io/ipfs/bafkreicmsjwlxi6ffcoor7xxyxsgyetkj6if3wjmw7hykgzexkvuisc7um";
  const linkUrl = "https://ipfs.io/ipfs/bafkreiajkzoobvlytisgbx5hw6eyul65fung7pdfz4vnoap7boo6yp66tq";

  let explorer, contractAddress;
  switch (network) {
    case 'bsc':
      explorer = process.env.BSC_BLOCKCHAIN_EXPLORER;
      contractAddress = process.env.BSC_CONTRACT_ADDRESS;
      break;
    case 'eth':
      explorer = process.env.ETHEREUM_BLOCKCHAIN_EXPLORER;
      contractAddress = process.env.ETHEREUM_CONTRACT_ADDRESS;
      break;
    case 'polygon':
      explorer = process.env.POLYGON_BLOCKCHAIN_EXPLORER;
      contractAddress = process.env.POLYGON_CONTRACT_ADDRESS;
      break;
    case 'near':
      explorer = process.env.NEAR_BLOCKCHAIN_EXPLORER;
      contractAddress = process.env.NEAR_CONTRACT_ADDRESS;
      break;
  }
  let mintTransactionLink, blockchainTokenUrlLink;

  if (network === 'near') {
    mintTransactionLink = `${explorer}/?query=${transactionHash}`;
    blockchainTokenUrlLink = mintTransactionLink;
  } else {
    mintTransactionLink = `${explorer}/tx/${transactionHash}`;
    blockchainTokenUrlLink = `${explorer}/token/${contractAddress}?a=${tokenId}`;
  }

  const [
    mintTransactionQrCodeLink,
    linkUrlQrCodeLink,
    metadataUrlQrCodeLink,
    blockchainTokenUrlQrCodeLink,
  ] = await Promise.all([
    generateAndUploadQr('tx', mintTransactionLink, ownerId, ownerId, network),
    generateAndUploadQr('source', linkUrl, ownerId, ownerId),
    generateAndUploadQr('meta', metadataUrl, ownerId, ownerId),
    generateAndUploadQr('token', blockchainTokenUrlLink, ownerId, ownerId, network),
  ]);

  console.log(`mintTransactionQrCodeLink: `, mintTransactionQrCodeLink);
  console.log(`linkUrlQrCodeLink: `, linkUrlQrCodeLink);
  console.log(`metadataUrlQrCodeLink: `, metadataUrlQrCodeLink);
  console.log(`blockchainTokenUrlQrCodeLink: `, blockchainTokenUrlQrCodeLink);
})();