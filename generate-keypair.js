const { randomBytes } = require('crypto')
const secp256k1 = require('secp256k1')
const msg = randomBytes(32);
const crypto = require('crypto');
let privKey
do {
  privKey = randomBytes(32)
} while (!secp256k1.privateKeyVerify(privKey))

const testMsg = JSON.stringify({'hello': 'world'});
const hash2 = Uint8Array.from(crypto.createHash('sha256').update(testMsg).digest());
const result = {};
result.privateKey = Uint8Array.from(privKey);
result.pubKey = secp256k1.publicKeyConvert(secp256k1.publicKeyCreate(privKey), false);

const sigObj = secp256k1.ecdsaSign(msg, privKey)
result.msg = JSON.stringify({
  msgArray: Array.from(msg),
  sigArray: Array.from(sigObj.signature),
  v: sigObj.recid,
  msgArray2: Array.from(hash2)
});

const fs = require('fs');
fs.writeFileSync('keypair.json', JSON.stringify(result));
console.log(result.msg)
console.log(JSON.stringify(Array.from(result.pubKey)))