const dotenv = require('dotenv');
dotenv.config();
const morgan = require('morgan');
const cors = require('cors');

const express = require('express');
const app = express();

require('./server/configs/mongoose');

app.use(cors());
app.options('*', cors());
app.use(express.json({limit: `1024mb`}));
app.use(express.urlencoded({ extended: false }));
app.use(morgan(':date[iso] :method :url :status :res[content-length] - :response-time ms'));

require('./server/api')(app);

// error handler
app.use(function (err, req, res, next) {
  console.log(err);
  return res.status(500).json({ message: "Unhandled internal error" });
});

module.exports = app;
