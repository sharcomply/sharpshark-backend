require('dotenv').config();
require('./server/configs/mongoose');
const Content = require('mongoose').models.Content;

(async() => {
  const contents = await Content.find({});
  for (let i = 0; i < contents.length; i++) {
    console.log(i);
    const networks = contents[i].mintedNetworks;
    if (networks.length === 0) {
      continue;
    }
    const ethIndex = networks.findIndex(item => item.name === 'polygon');
    if (ethIndex === -1) {
      continue;
    }
    contents[i].mintedNetworks.splice(ethIndex, 1);
    if (contents[i].mintedNetworks.length === 0) {
      contents[i].isMinted = false;
    }
    await contents[i].save();
  }
})();