require('dotenv').config();
// const recoverByWallet = require('./recover-by-wallet');
const tldjs = require('tldjs');

(async () => {
  // await recoverByWallet('sane5ek.testnet');
  // await recoverByWallet('0x085f110495D4832f2E299fa8d21f3c27f1129329'.toLowerCase());
  const str1 = 'https://test.influyentescantabria.es/sharp-shark-tecnologia-blockchain-aplicada-a-la-propiedad-intelectual/';
  const str2 = 'https://test.community.hackernoon.com/t/hustlegpt-one-mans-quest-to-let-gpt-4-run-his-business/60845 , ';
  const urlObject1 = new URL(str1);
  const urlObject2 = new URL(str2);
  let domainName1 = tldjs.getDomain(urlObject1.hostname);
  let domainName2 = tldjs.getDomain(urlObject2.hostname);
  console.log(domainName1);
  console.log(domainName2);
})();