require('dotenv').config();
const contracts = require('./server/blockchain/ethereum/events');
const crypto = require("crypto");
require('./server/configs/mongoose');
const User = require('mongoose').models.User;
const Content = require('mongoose').models.Content;
const {v4: uuidv4} = require("uuid");
const Promise = require("bluebird");
const mintContentNetwork = require("./server/utils/mint-content-network");
const FileType = require('file-type');
const nearAPI = require("near-api-js");
const axios = require("axios");
const moment = require('moment');
const uploadToS3 = require('./server/utils/upload-to-s3');

async function connectToNear() {
  const connectionConfig = {networkId: process.env.NEAR_NETWORK_ID, nodeUrl: process.env.NEAR_NODE_URL, walletUrl: process.env.NEAR_WALLET_URL, helperUrl: process.env.NEAR_HELPER_URL, explorerUrl: process.env.NEAR_EXPLORER_URL,};
  const nearConnection = await nearAPI.connect(connectionConfig);
  const account = await nearConnection.account();
  return new nearAPI.Contract(account, process.env.NEAR_CONTRACT_ADDRESS, {viewMethods: ["nft_token"], sender: account});
}

async function getIpfsData(hash, isBuffer, isFull) {
  console.log(`\t\tTrying to get data from ipfs.`);
  const prefixes = ['https://nftstorage.link/ipfs/', 'https://ipfs.io/ipfs/'];
  let prefix = 0;
  let result;
  while (!result) {
    try {
      result = await axios.get(prefixes[prefix] + hash, isBuffer ? {
        responseType: 'arraybuffer',
        headers: isFull ? undefined : {Range: `bytes=0-${100}`}
      } : undefined);
    } catch (e) {
      console.log(`\t\tIPFS not answered. Retrying... ${prefixes[prefix] + hash}`);
      prefix += prefix === (prefixes.length - 1) ? -prefix : 1;
      await new Promise(r => setTimeout(r, 500));
    }
  }
  return result;
}

module.exports = async (address) => {
  const isNear = !address.startsWith('0x');
  const networks = isNear ? ['near'] : ['eth', 'bsc', 'polygon'];
  const nearContract = await connectToNear();

  while (Object.keys(contracts).length !== 3) {
    await new Promise(r => setTimeout(r, 200));
  }

  const filter = isNear ? {walletAddressNear: address} : {walletAddress: address};
  const user = await User.findOneAndUpdate(filter, {nonce: crypto.randomBytes(32).toString('base64')}, {
    upsert: true,
    setDefaultsOnInsert: true
  });

  const createdContents = [];
  for (let network of networks) {
    // if (network === 'eth' || network === 'bsc') continue;
    console.log(`Checking address at ${network}.`);
    let url;
    if (isNear) {
      url = `${process.env.NEARBLOCKS_API_URL}/v1/account/${address}/txns?to=${process.env.NEAR_CONTRACT_ADDRESS}&method=nft_mint&page=1&per_page=25&order=desc`;
    } else {
      const scanUrl = network === 'bsc' ? process.env.BSC_SCAN_URL : (network === 'eth' ? process.env.ETHEREUM_SCAN_URL : process.env.POLYGON_SCAN_URL);
      const scanApiKey = network === 'bsc' ? process.env.BSC_SCAN_KEY : (network === 'eth' ? process.env.ETHEREUM_SCAN_KEY : process.env.POLYGON_SCAN_KEY);
      const contractAddress = network === 'bsc' ? process.env.BSC_CONTRACT_ADDRESS : (network === 'eth' ? process.env.ETHEREUM_CONTRACT_ADDRESS : process.env.POLYGON_CONTRACT_ADDRESS);
      url = `${scanUrl}/api?module=account&action=tokennfttx&contractaddress=${contractAddress}&address=${address}&sort=asc&apikey=${scanApiKey}`;
    }

    const scanRes1 = await axios.get(url);
    const tokens = [];
    const transactionHashes = [];
    const dates = [];
    if (isNear) {
      for (let i = 0; i < scanRes1.data.txns.length; i++) {
        const transaction = scanRes1.data.txns[i];
        const dateString = moment.unix(transaction.block_timestamp.substring(0, transaction.block_timestamp.length - 9));
        if (transaction.logs.length === 0) {
          continue;
        }
        const tokenId = transaction.logs[0].split('"token_ids\\":[\\"')[1].split('\\"')[0];
        tokens.push(tokenId);
        transactionHashes.push(transaction.transaction_hash);
        dates.push(dateString);
      }
    } else {
      for (let i = 0; i < scanRes1.data.result.length; i++) {
        const transaction = scanRes1.data.result[i];
        const dateString = moment.unix(transaction.timeStamp);
        if (transaction.to.toLowerCase() === address.toLowerCase()) {
          tokens.push(transaction.tokenID);
          transactionHashes.push(transaction.hash);
          dates.push(dateString);
        }
        if (transaction.from.toLowerCase() === address.toLowerCase()) {
          const index = tokens.indexOf(transaction.tokenID);
          if (index > -1) {
            tokens.splice(index, 1);
            transactionHashes.splice(index, 1);
            dates.splice(index, 1);
          }
        }
      }
    }

    console.log(`\tFound ${tokens.length} at ${network}.`);
    for (let i = 0; i < tokens.length; i++) {
      console.log(`\t\tStarting recreating ${i + 1} token at ${network}.`);
      let parentTokenId, version, metadataHash, contentHash;
      if (isNear) {
        const tokenInfo = await nearContract.nft_token({token_id: tokens[i]});
        parentTokenId = tokenInfo.parent_token_id;
        version = tokenInfo.version;
        metadataHash = tokenInfo.metadata_hash;
        contentHash = tokenInfo.content_hash;
      } else {
        const tokenInfo = await contracts[network].getTokenInfo(tokens[i]);
        parentTokenId = tokenInfo[3];
        version = tokenInfo[4];
        metadataHash = tokenInfo[5];
        contentHash = tokenInfo[6];
      }
      if (!metadataHash) {
        console.log(`metadata hash not found. skipping...`);
        continue;
      }
      const metadataRes = await getIpfsData(metadataHash);
      console.log(`\t\tGot metadata for ${i + 1} token at network ${network}.`);
      const metadata = metadataRes.data;
      const contentRes = await getIpfsData(contentHash);
      const c = await getIpfsData(contentHash, true);
      const originalType = await FileType.fromBuffer(c.data);
      const cutType = await FileType.fromBuffer(c.data.slice(3));
      const type = cutType || originalType;
      let content = await Content.findOne({tokenId: tokens[i]});
      if (!content) {
        console.log(`\t\tThat's new content`);
        content = new Content();
        content.mintedNetworks = [];
        content.commonId = uuidv4();
      }
      console.log(`\t\tUpdating content with tokenId=${tokens[i]}...`);
      console.log(`length: `, contentRes.data.length)
      content.type = type ? 'image' : 'text';
      content.isMinted = true;
      content.title = metadata.Title;
      content.fullTitle = metadata.Title;
      content.ownerId = user._id;
      content.authors = metadata.Authors;
      content.coAuthors = metadata.CoAuthors;
      content.participants = metadata.Participants;
      content.version = version;
      content.parentTokenId = parentTokenId;
      content.additionalImages = [];
      content.images = [];
      content.sanitizedLinkUrl = '';
      content.cachedMetadata = metadata;
      content.linkUrl = metadata.SourceIPFSUri;
      content.metadataHash = metadataHash;
      content.metadataUrl = 'https://ipfs.io/ipfs/' + metadataHash;
      content.sourceHash = contentHash;
      content.tokenId = tokens[i];
      content.linkUrlQrCodeLink = '';
      content.metadataUrlQrCodeLink = '';
      content.updatedAt = dates[i];
      content.createdAt = dates[i];

      if (contentRes.data.length > 1000000) {
        const contentResFull = await getIpfsData(contentHash, true, true);
        content.cachedSource = await uploadToS3(contentResFull.data, `${user._id}/${uuidv4()}.${type ? type.ext : 'jpg'}`);
      } else {
        content.cachedSource = contentRes.data
      }
      await mintContentNetwork(content, network, transactionHashes[i]);
      await content.save();
      await Content.updateOne({_id: content._id}, { createdAt: dates[i], updatedAt: dates[i] }, { timestamps: false });
      createdContents.push(content);
      console.log(`\t\tCreated new content with tokenId=${tokens[i]}.`);
    }
  }
  console.log(`Getting parent tokens...`);
  for (let i = 0; i < createdContents.length; i++) {
    if (createdContents[i].parentTokenId !== "0") {
      const parent = await Content.findOne({tokenId: createdContents[i].parentTokenId});
      if (!parent) {
        console.log(`\tParent not found for token ${createdContents[i].tokenId}`);
        continue;
      }
      createdContents[i].parentId = parent._id;
      parent.childId = createdContents[i]._id;
      await parent.save();
      await createdContents[i].save();
    }
  }
  console.log(`Done`);
}