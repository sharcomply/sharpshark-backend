function onWWWError(error, server){
  
  /*
  `server.address()` isn't reliable until the 'listening' event is
emitted because Node may need to do a DNS lookup when you pass in a
host name, the results of which aren't immediately available.
   Если порт in use, то server.address() вернёт null, потому что не смог опросить DNS,
   и зарегистрировать значение, поэтому присваиваем значение порта руками.
   */
  let serverPort = server.address();
  if(serverPort == null){
    console.log("server port is null");
    serverPort = process.env.PORT || '3000'
  } else {
    console.log("server port isn't null");
    serverPort = serverPort.port;
  }
  if ( error.syscall !== 'listen' ) {
    throw error;
  }
  
  let bind = typeof serverPort === 'string'
    ? 'Pipe ' + serverPort
    : 'Port ' + serverPort;
  
  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

module.exports = onWWWError;