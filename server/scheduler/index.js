const Agenda = require('agenda');
const path = require("path");
const fs = require("fs");
const agenda = new Agenda({ db: { address: process.env.AGENDA_MONGO_CONNECTION_STRING } });

const handlers = {};
const basename = path.basename(__filename);
fs.readdirSync(__dirname).filter(file => {
  return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
}).forEach(file => {
  handlers[file.split('.')[0]] = require(`${__dirname}/${file}`);
});

(async function () {
  await agenda.start();
  for (let handler in handlers) {
    await handlers[handler](agenda);
  }
})();

module.exports = agenda;