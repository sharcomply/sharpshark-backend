const copyleaks = require("../libraries/copyleaks");
const tineyeScan = require("../utils/tineye-scan");
const Monitoring = require('mongoose').models.Monitoring;
const ScanHistory = require('mongoose').models.ScanHistory;

module.exports = async (agenda) => {
  agenda.define("process content antiplag monitoring", async (job) => {
    const monitorings = await Monitoring.find({isActive: true}).populate('documentId');
    while (monitorings.length > 0) {
      const count = monitorings.length > 8 ? 7 : monitorings.length;
      for (let i = 0; i < count; i++) {
        if (!monitorings[i].documentId.isMinted) {
          console.log('not active');
          continue;
        }
        const newScan = new ScanHistory({
          ownerId: monitorings[i].documentId.ownerId,
          documentId: monitorings[i].documentId._id,
          purpose: 'monitoring',
          isRequestPayed: true,
        });
        await newScan.save();

        if (monitorings[i].documentId.type === 'image') {
          await tineyeScan(monitorings[i].documentId, newScan, 'monitoring');
        } else {
          await copyleaks.submitScan(monitorings[i].documentId, newScan);
        }
      }
      monitorings.splice(0, count);
      await new Promise(r => setTimeout(r, 2000));
    }

    console.log(`Finished task: process content antiplag monitoring. Active monitors = ${monitorings.length}`);
  });
  await agenda.every(process.env.SCHEDULER_MONITORING_CRON, "process content antiplag monitoring");
}