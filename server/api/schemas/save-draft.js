const Joi = require('joi');

module.exports = Joi.object({
  type: Joi.string(),
  linkUrl: Joi.string(),
  HashIpfs: Joi.string(),
  title: Joi.string(),
  fullTitle: Joi.string(),
  description: Joi.string(),
  isMinted: Joi.forbidden()
});
