'use strict';

const path = require('path');

module.exports = (app) => {
  require('./user')(app);
  require('./public')(app);
  require('./copyleaks')(app);
  require('./admin')(app);
  require('./shuftipro')(app);
  require('./stripe')(app);
};
