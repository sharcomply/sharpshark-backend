'use strict';

const getGlobbedFiles = require('../../utils/getGlobbedFiles');
const path = require('path');
const jwtAuth = require('../middlewares/jwt-auth');
const Controller = require('../Controller');

module.exports = (router) => {
  //TODO need to restrict calls to copyleaks only
  //TODO for now done using nonce in developerPayload
  //router.use('/copyleaks', jwtAuth);
  
  getGlobbedFiles(path.join(__dirname, './routes/*.js'))
  .forEach(path => {
    const controllerParams = require(path);
    let controller = new Controller(controllerParams);
    controller.register(router);
  });
};
