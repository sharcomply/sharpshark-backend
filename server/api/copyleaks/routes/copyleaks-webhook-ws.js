const ScanHistory = require('mongoose').models.ScanHistory;
const Content = require('mongoose').models.Content;
const ApplicationErrors = require('../../../utils/application-error')
const processScanForMint = require('../../../utils/process-scan-for-mint');
const processScanForMonitoring = require('../../../utils/process-scan-for-monitoring');
const connections = require("../../../websocket/event-subscribers/connected-sockets");
const tineye = require('../../../configs/tineye');
const Promise = require('bluebird');

module.exports = {
  method: 'post',
  path: '/api/v1/copyleaks/webhook-ws/:status',
  handler: async function (req, res) {
    processWebhook(req);
    return {};
  }
};

async function processWebhook(req) {
  let [parsedNonce, ownerId, purpose, network] = req.body.developerPayload.split('_');
  if (!parsedNonce) {
    throw ApplicationErrors.BadRequest();
  }
  if (req.body.status === 2) {
    const scan = await ScanHistory.findOne({nonceEstimate: parsedNonce}).select('+nonceEstimate');
    scan.pagesCount = req.body.credits;
    await scan.save();
    const content = await Content.findOne({_id: scan.documentId});
    content.pagesCount = req.body.credits;
    await content.save();
    const connection = connections[ownerId];
    if (connection) {
      connection.send(JSON.stringify({
        action: 'scanEstimateCurrentStatus',
        status: 'estimatedSuccess',
        scanId: scan._id,
        draftId: scan.documentId
      }));
      console.log(`Sent estimated success for uniqueId=${connection.uniqueId}`)
      return {};
    } else {
      console.log(`No connection for user ${ownerId}`)
      return {};
    }
  } else if (req.body.status === 1) {
    const connection = connections[ownerId];
    if (connection) {
      connection.send(JSON.stringify({
        action: 'scanEstimateCurrentStatus',
        status: 'estimatedError',
        error: req.body.error.message,
      }));
      console.log(`Sent estimated error for uniqueId=${connection.uniqueId}`)
      return {};
    } else {
      console.log(`No connection for user ${ownerId}`)
      return {};
    }
  } else {
    if (!req.body.results) {
      return {};
    }
    const aggregatedScore = req.body.results.score.aggregatedScore;
    const similarInternet = req.body.results.internet;
    const totalWords = req.body.scannedDocument.totalWords;

    const scan = await ScanHistory.findOne({_id: req.body.scannedDocument.scanId, nonce: parsedNonce})
      .select('+nonce').populate('documentId').populate('ownerId');
    if (scan.isCopyleaksWebhooked) {
      return {};
    }
    scan.isCopyleaksWebhooked = true;
    await scan.save();
    const content = await Content.findOne({_id: scan.documentId});

    if (!scan) {
      throw ApplicationErrors.NotFound();
    }
    scan.aggregatedScore = aggregatedScore;
    scan.similarContent = similarInternet.map(item => {
      return {
        url: item.url,
        title: item.title,
        introduction: item.introduction,
        similarity: item.matchedWords / totalWords * 100
      }
    });

    if (content.additionalImages) {
      for (let i = 0; i < content.additionalImages.length; i++) {
        let response;
        try {
          response = await tineye.searchUrl(content.additionalImages[i]);
        } catch (e) {
          console.log(`ADDITIONAL IMAGES ERROR: `, content.additionalImages[i]);
          continue;
        }
        let aggregatedScore = response.results.matches.length > 0 ? response.results.matches[0].score : 0;
        if (process.env.TINEYE_API_KEY === '6mm60lsCNIB,FwOWjJqA80QZHh9BMwc-ber4u=t^' && Math.random() < 0.9) {
          // if test api key, chance for bad score = 10%
          aggregatedScore = 0;
        }
        let similarContent = response.results.matches.map(item => {
          return {
            url: item.backlinks[0].url,
            title: item.domain,
            introduction: item.backlinks[0].backlink,
            similarity: item.score
          }
        });
        scan.aggregatedScore += aggregatedScore;
        scan.similarContent = scan.similarContent.concat(similarContent);
      }
    }

    await scan.save();
    if (purpose === 'mint') {
      await processScanForMint(scan, content, network);
    } else if (purpose === 'monitoring') {
      await processScanForMonitoring(scan);
    }
    return {};
  }
}
