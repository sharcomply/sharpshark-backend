const validate = require('../utils/validate');

const validMethod = {
  get: 'get',
  post: 'post',
  delete: 'delete',
  patch: 'patch',
  put: 'put'
}

class Controller {
  constructor({method, path, handler, middleware}) {
    this.method = validMethod[method];
    this.path = path;
    this.handler = handler;
    this.middleware = middleware;
  }
  
  getMethod() {
    return this.method;
  }
  
  getPath() {
    return this.path;
  }
  
  getHandler() {
    return this.handler;
  }

  getMiddleware() {
    return this.middleware;
  }
  
  register(router) {
    const self = this;
    const handle = async function (req, res) {
      try {
        validate(req);
        let handlerResult = await self.handler(req, res);
        if (!res._header) {
          return res.status(200).json({success: true, statusCode: 200, data: handlerResult})
        }
      } catch (e) {
        console.log(e);
        const statusCode = e.httpStatusCode ? e.httpStatusCode : 500;
        return res.status(statusCode).json({success: false, statusCode: statusCode, data: {message: e.message}});
      }
    };

    if (self.middleware) {
      router[self.method](self.path, self.middleware, handle);
    } else {
      router[self.method](self.path, handle);
    }
  }
}

module.exports = Controller;
