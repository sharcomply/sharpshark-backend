const ApplicationError = require("../../../utils/application-error");
const connections = require("../../../websocket/event-subscribers/connected-sockets");
const tineyeScan = require("../../../utils/tineye-scan");
const copyleaks = require("../../../libraries/copyleaks");
const ScanHistory = require('mongoose').models.ScanHistory;
const Content = require('mongoose').models.Content;
const Purchase = require('mongoose').models.Purchase;
const User = require('mongoose').models.User;
const Monitoring = require('mongoose').models.Monitoring;

module.exports = {
  method: 'post',
  path: '/api/v1/stripe/webhook',
  handler: async function (req, res) {
    if (!req.query.secret || req.query.secret !== process.env.JWT_SECRET) {
      throw ApplicationError.NotFound();
    }
    if (req.body.type === 'checkout.session.completed') {
      if (req.body.data.object.metadata.type === 'cert') {
        const user = await User.findOne({_id: req.body.data.object.metadata.ownerId}).lean();
        const purchase = new Purchase({
          email: user.email,
          walletAddress: user.walletAddress,
          amount: req.body.data.object.amount_total / 100
        });
        await purchase.save();
        return res.status(200).send();
      }
      if (req.body.data.object.metadata.type === 'scan') {
        const scan = await ScanHistory.findOne({requestId: req.body.data.object.metadata.requestId}).select('+nonce');
        const content = await Content.findOne({_id: scan.documentId});
        scan.isRequestPayed = true;
        await scan.save();
        const connection = connections[scan.ownerId];
        if (connection) {
          connection.send(JSON.stringify({
            action: 'scanCurrentStatus', // 1
            status: 'paymentReceived',
            scanId: scan._id,
            draftId: content._id,
          }));
          console.log(`Sent paymentReceived for uniqueId=${connection.uniqueId}`)
        } else {
          console.log(`No connection for user ${scan.ownerId}`)
        }

        if (content.type === 'image') {
          tineyeScan(content, scan, 'mint', req.body.data.object.metadata.network);
        } else {
          copyleaks.submitScan(content, scan, req.body.data.object.metadata.network);
        }
        return res.status(200).send();
      }
      if (req.body.data.object.metadata.type === 'monitoring') {
        const monitoring = await Monitoring.findOne({requestId: req.body.data.object.metadata.requestId});
        const content = await Content.findOne({_id: monitoring.documentId});
        monitoring.isPayed = true;
        monitoring.isActive = monitoring.scansTotal > 0;
        monitoring.scansLeft = monitoring.scansTotal;
        await monitoring.save();
        const connection = connections[content.ownerId];
        if (connection) {
          connection.send(JSON.stringify({
            action: 'monitoringStatus', // 1
            status: 'paymentReceived',
            draftId: content._id,
          }));
          console.log(`Sent paymentReceived for uniqueId=${connection.uniqueId}`)
        } else {
          console.log(`No connection for user ${content.ownerId}`)
        }
        return res.status(200).send();
      }
    }
    return res.status(200).send();
  }
};
