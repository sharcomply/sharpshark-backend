'use strict';

const getGlobbedFiles = require('../../utils/getGlobbedFiles');
const path = require('path');
const Controller = require('../Controller');

module.exports = (router) => {
  getGlobbedFiles(path.join(__dirname, './routes/*.js'))
  .forEach(path => {
    const controllerParams = require(path);
    let controller = new Controller(controllerParams);
    controller.register(router);
  });
};
