const SessionShuftipro = require('mongoose').models.SessionShuftipro;
const ApplicationError = require('../../../utils/application-error');
const User = require('mongoose').models.User;
const connections = require('../../../websocket/event-subscribers/connected-sockets');

module.exports = {
  method: 'post',
  path: '/api/v1/shuftipro/kyc-response',
  handler: async function (req, res) {
    const body = req.body;
    let session = await SessionShuftipro.findOne({generatedUserId: body.reference});
    if (!session) {
      throw ApplicationError.NotFound();
    }
    let communicationChannel = connections[session.userId];
    communicationChannel.send(JSON.stringify({action: "verificationWebhookEvent", event: body.event}))
    if (body.event === 'verification.accepted') {
      let user = await User.findOneAndUpdate({_id: session.userId}, {isVerification: true});
      let newToken = user.getJWT();
      if (communicationChannel) {
        communicationChannel.send(JSON.stringify({isVerification: true, data: {newToken: newToken}}));
      }
    }
    if (body.event === 'verification.declined') {
      let result = {
        declined_reason: body.declined_reason,
        declined_codes: body.declined_codes,
      }
      if (communicationChannel) {
        communicationChannel.send(JSON.stringify({isVerification: false,data: {result}}));
      }
    }
    session.responseRequest = req.body;
    await session.save();
    return res.status(200).end();
  }
}
