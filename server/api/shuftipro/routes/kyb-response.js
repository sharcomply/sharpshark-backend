const SessionShuftipro = require('mongoose').models.SessionShuftipro;
const ApplicationError = require('../../../utils/application-error');
const User = require('mongoose').models.User;
const connections = require('../../../websocket/event-subscribers/connected-sockets');

module.exports = {
  method: 'post',
  path: '/api/v1/shuftipro/kyb-response',
  handler: async function (req, res) {
    const body = req.body;
    let session = await SessionShuftipro.findOne({generatedUserId: body.reference});
    if (!session) {
      throw ApplicationError.NotFound();
    }
    if(body["verification_data"]["kyb"].length > 0) {
      connections[session.userId].send(JSON.stringify({isVerification: true, data: body["verification_data"]["kyb"]}));
    } else {
      connections[session.userId].send(JSON.stringify({isVerification: false,data: {body}}));
    }
    session.responseRequest = req.body;
    await session.save();
    return res.status(200).end();
  }
}
