const Purchase = require('mongoose').models.Purchase;

module.exports = {
  method: 'get',
  path: '/api/v1/admin/get-purchases',
  handler: async function (req, res) {
    const queryParams = req.query;
    if (req.query.search) {
      const regex = { $regex: '.*' + req.query.search + '.*', $options : 'ui' }
      queryParams.$or = [
        {'email': regex},
        {'walletAddress': regex},
        {'status': regex},
      ]
    }
    const sortParams = req.query.sort ? JSON.parse(req.query.sort) : [];
    delete queryParams.sort;
    delete queryParams.search;

    const paginationParams = {skip: req.query.skip, limit: req.query.limit};
    const items = await Purchase.find(queryParams, {}, paginationParams).sort(sortParams).lean();
    const count = await Purchase.countDocuments(queryParams);
    return {items, count}
  }
};