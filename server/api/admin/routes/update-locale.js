const Locale = require('mongoose').models.Locale;

module.exports = {
  method: 'patch',
  path: '/api/v1/admin/update-locale/:id',
  handler: async function (req, res) {
    const locale = await Locale.findOneAndUpdate({_id: req.params.id}, req.body, {returnDocument: "after", returnNewDocument: true})
    return {item: locale};
  }
};