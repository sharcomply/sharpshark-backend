const Locale = require('mongoose').models.Locale;

module.exports = {
  method: 'get',
  path: '/api/v1/admin/get-locales',
  handler: async function (req, res) {
    const queryParams = req.query;
    if (req.query.search) {
      const regex = { $regex: '.*' + req.query.search + '.*', $options : 'ui' }
      queryParams.$or = [
        {'name': regex},
        {'link': regex},
      ]
    }
    const sortParams = req.query.sort ? JSON.parse(req.query.sort) : [];
    delete queryParams.sort;
    delete queryParams.search;

    const paginationParams = {skip: req.query.skip, limit: req.query.limit};
    const items = await Locale.find(queryParams, {}, paginationParams).sort(sortParams).lean();
    const count = await Locale.countDocuments(queryParams);
    return {items, count}
  }
};