const Locale = require('mongoose').models.Locale;

module.exports = {
  method: 'delete',
  path: '/api/v1/admin/delete-locale/:id',
  handler: async function (req, res) {
    const locale = await Locale.findOneAndDelete({_id: req.params.id},{returnDocument: "before", returnNewDocument: true})
    return {item: locale};
  }
};