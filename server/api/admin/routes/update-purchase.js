const Purchase = require('mongoose').models.Purchase;

module.exports = {
  method: 'patch',
  path: '/api/v1/admin/update-purchase/:id',
  handler: async function (req, res) {
    const updatePurchase = await Purchase.findOneAndUpdate({_id: req.params.id}, req.body,
      {returnDocument: "after", returnNewDocument: true});
    return {item: updatePurchase};
  }
};
