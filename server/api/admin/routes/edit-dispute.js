const Dispute = require('mongoose').models.Dispute;
const ApplicationError = require('../../../utils/application-error');

module.exports = {
  method: 'post',
  path: '/api/v1/admin/edit-dispute/:id',
  handler: async function (req, res) {
    delete req.body.alertId;
    delete req.body.contentId;
    delete req.body.discoveredDate;
    delete req.body.ownerId;
    delete req.body.appearedOn;

    // if (req.body.status && ![
    //   'Claim Sent: Website Owner',
    //   'Claim Sent: Provider',
    //   'Claim Sent: Google',
    //   'Closed',
    //   'Under Court'
    // ].includes(req.body.status)) {
    //   throw ApplicationError.BadRequest();
    // }

    const paramsQuery = {
      _id: req.params.id,
    };
    return Dispute.findOneAndUpdate(paramsQuery, req.body,
      {returnDocument: "after", returnNewDocument: true});
  }
};

