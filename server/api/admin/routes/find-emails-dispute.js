const Dispute = require('mongoose').models.Dispute;
const ApplicationError = require('../../../utils/application-error');
const axios = require('axios');
const tldjs = require('tldjs');

module.exports = {
  method: 'post',
  path: '/api/v1/admin/find-emails-dispute/:id',
  handler: async function (req, res) {
    const dispute = await Dispute.findOne({_id: req.params.id}).lean();
    if (!dispute) {
      throw ApplicationError.NotFound();
    }
    const urlObject = new URL(dispute.appearedOn);
    let domainName = tldjs.getDomain(urlObject.hostname);
    let response, emails;
    try {
      response = await axios.get(`https://api.hunter.io/v2/domain-search?domain=${domainName}&api_key=${process.env.HUNTER_API_KEY}`);
      emails = response.data.data.emails.map(item => item.value);
    } catch (e) {
      console.log(e.response.data);
      throw ApplicationError.MailFindError();
    }

    return {emails};
  }
};

