const AWS = require("aws-sdk");
const { v4: uuidv4 } = require('uuid');
const mime = require('mime-types');
const Dispute = require('mongoose').models.Dispute;

const client = new AWS.S3({
  signatureVersion: 'v4',
  region: process.env.S3_REGION || 'eu-west-1'
});

module.exports = {
  method: 'get',
  path: '/api/v1/admin/get-image-upload-link/:disputeId',
  handler: async function (req, res) {
    const dispute = await Dispute.findOne({_id: req.params.disputeId}).lean();
    const filename = `${dispute.ownerId}/${dispute.documentId}_${uuidv4()}.${req.query.fileExt}`;
    const bucket = process.env.S3_BUCKET_NAME;
    const params = {
      Bucket: bucket,
      Key: filename,
      Fields: {
        key: filename,
      },
      Expires: 30 * 60, // 30 minutes
    };
    params['Fields']['Content-Type'] = mime.lookup(filename);
    params['Fields']['Cache-Control'] = 'no-cache';
    const url = await client.createPresignedPost(params);
    return { uploadLink: url, downloadLink: `https://${process.env.CDN_NAME}/` + url.fields.key};
  }
};
