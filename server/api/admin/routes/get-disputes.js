const ObjectId = require('mongoose').Types.ObjectId;
const Dispute = require('mongoose').models.Dispute;

module.exports = {
  method: 'get',
  path: '/api/v1/admin/get-disputes',
  handler: async function (req, res) {
    const queryParams = req.query;
    const paginationParams = {skip: req.query.skip ? Number.parseInt(req.query.skip) : 0, limit: req.query.limit ? Number.parseInt(req.query.limit) : 99999999};
    if (queryParams.status) {
      queryParams.status = {$in: queryParams.status.split(',')}
    }
    if (queryParams.documentId) {
      queryParams.documentId = new ObjectId(queryParams.documentId)
    }
    if (req.query.isNotRelevant) {
      queryParams.isNotRelevant = true;
    } else {
      queryParams.isNotRelevant = {$ne: !req.query.isNotRelevant};
    }
    let sortArray = [['createdAt', -1]]
    if (req.query.sort) {
      sortArray = JSON.parse(req.query.sort)
    }
    let sort = {};
    sortArray.forEach(arr => {
      sort[arr[0]] = arr[1];
    })

    let searchObj;
    if (req.query.search) {
      searchObj = {$match: {'documentId.title': { $regex: '.*' + decodeURIComponent(req.query.search) + '.*', $options : 'ui' }}}
    }
    delete queryParams.sort;
    delete queryParams.skip;
    delete queryParams.limit;
    delete queryParams.search;

    const aggregatePipeline = [
      { $match: queryParams },
      { $lookup : { from: "contents", localField: "documentId", foreignField: "_id", as: "documentId" } },
      { $unwind: '$documentId' },
      { $lookup : { from: "alerts", localField: "alertId", foreignField: "_id", as: "alertId" } },
      { $unwind: '$alertId' },
      { $lookup : { from: "scanhistories", localField: "alertId.scanId", foreignField: "_id", as: "alertId.scanId" } },
      { $unwind: '$alertId.scanId' },
    ];
    if (searchObj) {
      aggregatePipeline.push(searchObj)
    }

    const aggregatePipelineWithPaginationAndSorts = [...aggregatePipeline];
    aggregatePipelineWithPaginationAndSorts.push({$sort: sort});
    aggregatePipelineWithPaginationAndSorts.push({$skip: paginationParams.skip});
    aggregatePipelineWithPaginationAndSorts.push({$limit: paginationParams.limit});

    const aggregatePipelineCount = [...aggregatePipeline];
    aggregatePipelineCount.push({$count: 'count'});

    let items = await Dispute.aggregate(aggregatePipelineWithPaginationAndSorts, {collation: {locale: 'en'}});
    let count = await Dispute.aggregate(aggregatePipelineCount);
    return {items, count: count[0]?.count ? count[0].count : 0}
  }
};
