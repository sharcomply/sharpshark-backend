const AWS = require("aws-sdk");

const client = new AWS.S3({
  signatureVersion: 'v4',
  region: process.env.S3_REGION || 'eu-west-1'
});

module.exports = {
  method: 'get',
  path: '/api/v1/admin/get-locale-file-upload-link',
  handler: async function (req, res) {
    const filename = `locales/${req.query.name}`;
    const bucket = process.env.S3_BUCKET_NAME;
    const params = {
      Bucket: bucket,
      Key: filename,
      Fields: {
        key: filename,
      },
      Expires: 30 * 60, // 30 minutes
    };
    params['Fields']['Cache-Control'] = 'no-cache';
    const url = await client.createPresignedPost(params);
    return { uploadLink: url, downloadLink: `https://${process.env.CDN_NAME}/` + url.fields.key};
  }
};
