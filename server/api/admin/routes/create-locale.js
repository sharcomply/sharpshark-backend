const Locale = require('mongoose').models.Locale;

module.exports = {
  method: 'post',
  path: '/api/v1/admin/create-locale',
  handler: async function (req, res) {
    const locale = new Locale({name: req.body.name, link: req.body.link});
    await locale.save();
    return {item: locale};
  }
};