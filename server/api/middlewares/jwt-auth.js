'use strict';

const jwt = require('jsonwebtoken');

if (!process.env.JWT_SECRET) {
  console.log("NO JWT_SECRET set in the ENV");
  process.exit(-1);
}

module.exports = (req, res, next) => {
  let token = req.header('Authorization');
  try {
    let payload = jwt.verify(token, process.env.JWT_SECRET);
    req.jwt = payload;
    next();
  } catch (e) {
    // console.log(e);
    return res.status(401).json({message: "JWT token is not valid"});
  }
};