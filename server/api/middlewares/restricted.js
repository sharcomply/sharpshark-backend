'use strict';

module.exports = (req, res, next) => {
  let secret = req.header('Restricted');
  if (secret !== process.env.RESTRICTED_SECRET) {
    return res.status(401).json({message: "API usage is restricted"});
  } else {
    next();
  }
};