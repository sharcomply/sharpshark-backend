const jwt = require('jsonwebtoken');
const User = require('mongoose').models.User;
const ApplicationError = require('../../../utils/application-error');

module.exports = {
  method: 'get',
  path: '/api/v1/public/email',
  handler: async function (req, res) {
    const data = jwt.verify(req.query.token, process.env.JWT_SECRET);
    const user = await User.findOne({
      _id: data._id,
      email: data.email
    });
    if (!user) {
      throw ApplicationError.NotFound();
    }
    user.isEmailVerified = true;
    await user.save();
    return {isEmailVerified: true, email: user.email};
  }
};
