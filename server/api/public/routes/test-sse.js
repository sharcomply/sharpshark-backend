'use strict';
const connectedSockets = require('../../../sse/connected-sockets')

module.exports = {
  method: 'get',
  path: '/api/v1/public/test-sse',
  handler: async function (req, res) {
    for (const [userId, userRes] of Object.entries(connectedSockets)) {
      userRes.res.write(`data: ${JSON.stringify({test: "123"})}\n\n`)
    }
    return {};
  }
};
