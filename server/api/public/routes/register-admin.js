const Admin = require('mongoose').models.Admin;
const ApplicationError = require('../../../utils/application-error');

module.exports = {
  method: 'post',
  path: '/api/v1/public/register-admin',
  handler: async function (req, res) {
    if (req.query.secret !== process.env.JWT_SECRET) {
      throw ApplicationError.NoPermission();
    }
    const oldAdmin = await Admin.findOne({email: req.body.email});
    if (oldAdmin) {
      throw ApplicationError.AlreadyExists();
    }
    const admin = new Admin({email: req.body.email, password: req.body.password});
    await admin.save();
    return {};
  }
};
