const Promise = require("bluebird");
const convertFromWeiToUsd = require("../../../utils/convert-from-wei-to-usd");

module.exports = {
  method: 'get',
  path: '/api/v1/public/convert-from-wei',
  handler: async function (req, res) {
    const networks = ['bsc', 'eth', 'polygon', 'near'];
    const converted = await Promise.all(networks.map(
      item => convertFromWeiToUsd(req.query.amount, item)
    ));
    return {
      'bsc': converted[0],
      'eth': converted[1],
      'polygon': converted[2],
      'near': converted[3],
    };
  }
};
