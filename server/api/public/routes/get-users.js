const User = require('mongoose').models.User;
const ApplicationError = require('../../../utils/application-error');

module.exports = {
  method: 'get',
  path: '/api/v1/public/users',
  handler: async function (req, res) {
    if (req.query.address) {
      const user = await User.findOne({walletAddress: req.query.address.toLowerCase()});
      if (!user) {
        throw ApplicationError.NotFound();
      }
      return {userId: user._id};
    }
    throw ApplicationError.NotFound();
  }
};
