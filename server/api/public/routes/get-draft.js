const Content = require('mongoose').models.Content;

module.exports = {
  method: 'get',
  path: '/api/v1/public/get-drafts/:id',
  handler: async function (req, res) {
    const id = req.params.id;
    let draft;
    if (id.match(/^[0-9]+$/i)) {
      draft = await Content.findOne({tokenId: id}).lean();
    } else {
      draft = await Content.findOne({_id: id}).lean();
    }

    if (!draft) {
      return draft;
    }
    draft.versionsHistory = {};
    draft.versionsHistoryObjects = {};
    draft.versionsHistory[draft.version] = draft._id;
    draft.versionsHistoryObjects[draft.version] = await Content.findOne({_id: draft._id}).lean();
    let childId = draft.childId;
    let parentId = draft.parentId;
    while (childId) {
      const newDraft = await Content.findOne({_id: childId}).lean();
      draft.versionsHistory[newDraft.version] = newDraft._id;
      draft.versionsHistoryObjects[newDraft.version] = newDraft;
      childId = newDraft.childId;
    }
    while (parentId) {
      const newDraft = await Content.findOne({_id: parentId}).lean();
      draft.versionsHistory[newDraft.version] = newDraft._id;
      draft.versionsHistoryObjects[newDraft.version] = newDraft;
      parentId = newDraft.parentId;
    }
    return draft;
  }
};