const Content = require('mongoose').models.Content;
const User = require('mongoose').models.User;
const ApplicationError = require('../../../utils/application-error');
const contracts = require('../../../blockchain/ethereum/events');
const moment = require('moment');
const stripe = require("../../../configs/stripe");
const convertFromWeiToUsd = require("../../../utils/convert-from-wei-to-usd");
const nearAPI = require("near-api-js");

const contractsAddresses = {
  'bsc': process.env.BSC_CONTRACT_ADDRESS,
  'eth': process.env.ETHEREUM_CONTRACT_ADDRESS,
  'polygon': process.env.POLYGON_CONTRACT_ADDRESS,
  'near': process.env.NEAR_CONTRACT_ADDRESS,
}

module.exports = {
  method: 'post',
  path: '/api/v1/public/get-graft-cert-payment-link/:id',
  handler: async function (req, res) {
    //сделали так, потому что согласовали, что нужна цельная оплата фиатом,
    // и альтернатива токену в фиате - это чек
    const content = await Content.findOne({_id: req.params.id}).lean();
    const owner = await User.findOne({_id: content.ownerId}).lean();
    if (!content || !content.isMinted) {
      throw ApplicationError.NotFound();
    }
    const mintedNetwork = content.mintedNetworks.find(item => item.name === req.body.network);
    if (!mintedNetwork) {
      throw ApplicationError.NotFound();
    }

    let contractAddress, licenseHash, priceInUsd, walletAddress;
    if (req.body.network === 'near') {
      const connectionConfig = {networkId: process.env.NEAR_NETWORK_ID, nodeUrl: process.env.NEAR_NODE_URL, walletUrl: process.env.NEAR_WALLET_URL, helperUrl: process.env.NEAR_HELPER_URL, explorerUrl: process.env.NEAR_EXPLORER_URL,};
      const nearConnection = await nearAPI.connect(connectionConfig);
      const account = await nearConnection.account();
      const contract = new nearAPI.Contract(account, process.env.NEAR_CONTRACT_ADDRESS, {viewMethods: ["nft_token"], sender: account});
      const response = await contract.nft_token({token_id: content.tokenId});
      contractAddress = process.env.NEAR_CONTRACT_ADDRESS;
      licenseHash = response.licenseHash;
      priceInUsd = await convertFromWeiToUsd(response.sale_amount, mintedNetwork.name);
      walletAddress = owner.walletAddressNear;
    } else {
      const contract = contracts[mintedNetwork.name];
      const tokenInfo = await contract.getTokenInfo(content.tokenId);
      if (!tokenInfo[9] || !tokenInfo[7]) {
        throw ApplicationError.TokenIsNotOnSale();
      }
      contractAddress = contractsAddresses[mintedNetwork.name];
      licenseHash = tokenInfo[7];
      priceInUsd = await convertFromWeiToUsd(tokenInfo[0].toString(10), mintedNetwork.name);
      walletAddress = owner.walletAddress;
    }

    const description = `
      Blockchain: ${mintedNetwork.name}, 
      Contract Address: ${contractAddress}, 
      Metadata IPFS Hash: ${content.metadataHash}, 
      License IPFS Hash: ${licenseHash}, 
      Content IPFS Hash: ${content.sourceHash}, 
      Buy Date: ${moment().format('YYYY/MM/DD HH:mm:ss')}, 
      Expire Date: ${moment().add(28, 'days').format('YYYY/MM/DD HH:mm:ss')}, 
      Seller: ${walletAddress}
    `;
    const product = await stripe.products.create({
      name: `Certificate for token ${content.tokenId}`,
    });
    const price = await stripe.prices.create({
      unit_amount: Number.parseInt((priceInUsd * 100).toString()),
      currency: 'usd',
      product: product.id,
    });
    const sharpSharkFeeUsd = priceInUsd * 5;
    const minPaymentPrice = Number.parseFloat(process.env.MINIMUM_CERTIFICATE_PAYMENT || 1);
    const minimumPrice = (minPaymentPrice - (priceInUsd + sharpSharkFeeUsd)) * 100;

    const items = [
      {price: price.id, quantity: 1},
      {
        price_data: {
          currency: 'usd',
          product_data: {
            name: 'SharpShark Fee'
          },
          unit_amount: Number.parseInt(sharpSharkFeeUsd.toString())
        },
        quantity: 1
      }
      // {
      //   price_data: {
      //     currency: 'usd',
      //     product_data: {
      //       name: 'Blockchain Transfer Fee Price'
      //     },
      //     unit_amount: 1500
      //   },
      //   quantity: 1
      // }
    ]
    if (priceInUsd < minPaymentPrice) {
      items.push({
        price_data: {
          currency: 'usd',
          product_data: {
            name: 'Minimum Price'
          },
          unit_amount: minimumPrice
        },
        quantity: 1
      })
    }
    const session = await stripe.checkout.sessions.create({
      success_url: req.body.successUrl ? req.body.successUrl : 'https://example.com/success',
      cancel_url: req.body.cancelUrl ? req.body.cancelUrl : 'https://example.com/cancel',
      mode: 'payment',
      line_items: items,
      metadata: {
        contentId: req.params.id,
        ownerId: content.ownerId.toString(),
        type: 'cert'
      },
      payment_intent_data: {
        description: description
      }
    });

    return {url: session.url};
  }
};