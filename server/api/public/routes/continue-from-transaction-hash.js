const { providers } = require("near-api-js");
const ApplicationError = require('../../../utils/application-error');
const nft_mint = require('../../../blockchain/near/events/nft_mint');
const nft_request_payed = require('../../../blockchain/near/events/nft_request_payed');
const nft_monitoring_request_payed = require('../../../blockchain/near/events/nft_monitoring_request_payed');

const provider = new providers.JsonRpcProvider(
  "https://archival-rpc.testnet.near.org"
);

module.exports = {
  method: 'get',
  path: '/api/v1/public/continue-from-transaction-hash/:transactionHash',
  handler: async function (req, res) {
    const result = await provider.txStatus(req.params.transactionHash, req.query.accountId);
    if (!result || !result.receipts_outcome) {
      throw ApplicationError.NotFound();
    }
    const logs = [];
    for (let i = 0; i < result.receipts_outcome.length; i++) {
      result.receipts_outcome[i].outcome.logs.forEach(log => {
        if (log.startsWith('EVENT_JSON')) {
          const new_log = JSON.parse(log.split('EVENT_JSON:')[1]);
          logs.push(new_log)
        }
      });
    }
    for (let i = 0; i < logs.length; i++) {
      let func = () => {};
      switch(logs[i].event) {
        case 'nft_mint':
          func = nft_mint;
          break;
        case 'nft_request_payed':
          func = nft_request_payed;
          break;
        case 'nft_monitoring_request_payed':
          func = nft_monitoring_request_payed;
          break;
      }
      func(req.params.transactionHash, logs[i].data[0])
    }
    return logs;
  }
};