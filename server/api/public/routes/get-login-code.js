'use strict';
const User = require('mongoose').models.User;
const crypto = require('crypto');

module.exports = {
  method: 'get',
  path: '/api/v1/public/get-login-code',
  handler: async function (req, res) {
    let code = crypto.randomBytes(32).toString('base64');
    await User.findOneAndUpdate(
      {walletAddress: req.query.address},
      {nonce: code},
      {upsert: true, setDefaultsOnInsert: true});
    return {codeToSign: code};
  }
};
