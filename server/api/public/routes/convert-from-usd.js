const convertFromUsdToWei = require('../../../utils/convert-from-usd-to-wei');
const Promise = require('bluebird');

module.exports = {
  method: 'get',
  path: '/api/v1/public/convert-from-usd',
  handler: async function (req, res) {
    const networks = ['bsc', 'eth', 'polygon', 'near'];
    const converted = await Promise.all(networks.map(
      item => convertFromUsdToWei(req.query.amount, item)
    ));
    return {
      'bsc': converted[0],
      'eth': converted[1],
      'polygon': converted[2],
      'near': converted[3],
    };
  }
};
