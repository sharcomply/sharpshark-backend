const Locale = require('mongoose').models.Locale;

module.exports = {
  method: 'get',
  path: '/api/v1/public/get-locales',
  handler: async function (req, res) {
    const queryParams = req.query;
    const items = await Locale.find(queryParams).lean();
    const count = await Locale.countDocuments(queryParams);
    return {items, count}
  }
};