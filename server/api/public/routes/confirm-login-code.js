'use strict';
const User = require('mongoose').models.User;
const blockchainUtil = require('../../../libraries/blockchain-util');

module.exports = {
  method: 'post',
  path: '/api/v1/public/confirm-login-code',
  handler: async function (req, res) {
    let {code, signature, address} = req.body;
    let user = await User.findOne({
      walletAddress: address
    });

    // TODO: remove before production
    if (user && req.query.secret === process.env.JWT_SECRET) {
      let { authToken } = user.getJWT();
      let websocket = `${process.env.WS_PROTOCOL}://${process.env.SERVER_IP}/ws?token=${authToken}`;
      return {authToken: authToken, websocket: websocket};
    }
    // ------------------------------

    if (process.env.NODE_ENV !== "production" && (!user || user.nonce !== code)) {
      return res.status(400).json({message: 'Code is not valid'})
    }
    let validationResult = blockchainUtil.isSignatureValid(code, signature, address);
    if (!validationResult) {
      return res.status(401).json({message: 'Signature does not match address and code provided'})
    }
  
    let { authToken } = user.getJWT();
    let websocket = `${process.env.WS_PROTOCOL}://${process.env.SERVER_IP}/ws?token=${authToken}`;
  
    return {authToken: authToken, websocket: websocket};
  }
};
