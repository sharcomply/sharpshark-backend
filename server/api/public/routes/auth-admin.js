const Admin = require('mongoose').models.Admin;
const ApplicationError = require('../../../utils/application-error');

module.exports = {
  method: 'post',
  path: '/api/v1/public/auth-admin',
  handler: async function (req, res) {
    const superAdmin = await Admin.findOne({email: req.body.email});
    if (!superAdmin) {
      const sa = new Admin({email: 'admin@admin.com', password: process.env.SA_PASSWORD});
      await sa.save();
    }

    const admin = await Admin.findOne({email: req.body.email}).select('+password +salt');
    if (!admin) {
      throw ApplicationError.NotFound();
    }
    const {authToken} = await admin.authenticate(req.body.password);
    return {authToken: authToken};
  }
};
