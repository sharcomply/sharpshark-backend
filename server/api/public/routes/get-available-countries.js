const countries = require('../../../utils/available-countries')

module.exports = {
  method: 'get',
  path: '/api/v1/public/get-countries',
  handler: async function (req, res) {
    return countries;
  }
};