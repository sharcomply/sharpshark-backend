const User = require('mongoose').models.User;
const ApplicationError = require('../../../utils/application-error');
const axios = require('axios');

module.exports = {
  method: 'post',
  path: '/api/v1/public/users',
  handler: async function (req, res) {
    if (req.body.address) {
      const user = await User.findOne({walletAddress: req.body.address.toLowerCase()});
      if (!user) {
        throw ApplicationError.NotFound();
      }
      setTimeout(async() => {
        await User.updateOne({walletAddress: req.body.address.toLowerCase()}, {isVerification: true});
        try {
          await axios.post(
            `${process.env.HTTP_PROTOCOL}://${process.env.SERVER_IP}/new-message`,
            {address: req.body.address.toLowerCase()}
          )
        } catch (e) {
          // pass
        }

      }, Math.floor(Math.random() * (5*60*1000 - 60*1000 + 1)) + 60*1000);
      return {userId: user._id};
    }
    throw ApplicationError.NotFound();
  }
};
