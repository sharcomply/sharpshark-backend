'use strict';

const getGlobbedFiles = require('../../utils/getGlobbedFiles');
const path = require('path');
const jwtAuth = require('../middlewares/jwt-auth');
const Controller = require('../Controller');

module.exports = (router) => {
  router.use('/api/v1/user/', jwtAuth);
  
  getGlobbedFiles(path.join(__dirname, './routes/*.js'))
  .forEach(path => {
    const controllerParams = require(path);
    let controller = new Controller(controllerParams);
    controller.register(router);
  });
};
