const ApplicationErrors = require('../../../utils/application-error')
const ScanHistory = require('mongoose').models.ScanHistory;
const Content = require('mongoose').models.Content;
const stripe = require('../../../configs/stripe');
const estimateContentScanPrice = require("../../../utils/estimate-content-scan-price");

module.exports = {
  method: 'post',
  path: '/api/v1/user/get-scan-payment-link',
  handler: async function (req, res) {
    const scan = await ScanHistory.findOne({requestId: req.body.requestId}).select('+nonce');
    if (!scan) {
      throw ApplicationErrors.NotFound();
    }
    const content = await Content.findOne({_id: scan.documentId});
    const priceInUsd = await estimateContentScanPrice(content);

    const items = [];
    if (content.type === 'image') {
      items.push({price: process.env.STRIPE_SCAN_IMAGE_PRICE_ID, quantity: 1});
    } else {
      if (content.pagesCount === -1) {
        throw ApplicationErrors.NotFound();
      }
      items.push({price: process.env.STRIPE_SCAN_TEXT_PRICE_ID, quantity: content.pagesCount});
      if (content.additionalImages.length > 0) {
        items.push({price: process.env.STRIPE_SCAN_IMAGE_PRICE_ID, quantity: content.additionalImages.length});
      }
    }

    const sharpSharkFeeUsd = (priceInUsd / 2) * 100;
    items.push({
      price_data: {
        currency: 'usd',
        product_data: {
          name: 'SharpShark Fee'
        },
        unit_amount: Number.parseInt(sharpSharkFeeUsd.toString())
      },
      quantity: 1
    })
    const minPaymentPrice = Number.parseFloat(process.env.MINIMUM_SCAN_PAYMENT || 1);
    const minimumPrice = (minPaymentPrice - priceInUsd) * 100;
    if (priceInUsd < minPaymentPrice) {
      items.push({
        price_data: {
          currency: 'usd',
          product_data: {
            name: 'Minimum Price'
          },
          unit_amount: minimumPrice
        },
        quantity: 1
      })
    }

    const session = await stripe.checkout.sessions.create({
      success_url: req.body.successUrl ? req.body.successUrl : 'https://example.com/success',
      cancel_url: req.body.cancelUrl ? req.body.cancelUrl : 'https://example.com/cancel',
      line_items: items,
      mode: 'payment',
      metadata: {
        requestId: req.body.requestId,
        type: 'scan',
        network: req.body.network
      }
    });

    return {url: session.url};
  }
};
