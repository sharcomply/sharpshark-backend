const Content = require('mongoose').models.Content;

module.exports = {
  method: 'post',
  path: '/api/v1/user/recover-draft/:id',
  handler: async function (req, res) {
    const thirtyDaysAgo = new Date();
    thirtyDaysAgo.setDate(thirtyDaysAgo.getDate()-30);
    const paramsQuery = {
      _id: req.params.id,
      ownerId: req.jwt.id,
      isDeleted: true,
      deleteDate: {$gt: thirtyDaysAgo}
    };
    await Content.updateOne(paramsQuery, {isDeleted: false, deleteDate: null});
    return {}
  }
};
