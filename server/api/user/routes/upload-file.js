const upload = require('multer')();
const uploadToNftStorage = require('../../../utils/upload-to-nft-storage');

module.exports = {
  method: 'post',
  path: '/api/v1/user/upload-file',
  middleware: upload.single('file'),
  handler: async function (req, res) {
    const cid = await uploadToNftStorage(req.file.buffer);
    return {url: 'https://ipfs.io/ipfs/' + cid.toString(), hash: cid.toString()};
  }
};
