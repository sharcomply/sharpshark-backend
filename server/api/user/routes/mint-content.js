const Content = require('mongoose').models.Content;

module.exports = {
  method: 'post',
  path: '/api/v1/user/mint-request/:id',
  handler: async function (req, res) {
    const contentId = req.params.id;
    const userId = req.jwt.id;
    const content = await Content.findOne({_id: contentId, ownerId: userId});
    //результат проверки контракта на блокчейне
    const isBlockchainContract = true;
    if (!isBlockchainContract) {
      throw new Error('Проверка подписи контракта на блокчейне не пройдена');
    }
    //подпись документа
    const isSignature = true;
    if (!isSignature) {
      throw new Error('Подпись документа не прошла');
    }
    return content;
  }
};
