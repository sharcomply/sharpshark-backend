const Content = require('mongoose').models.Content;
const ApplicationErrors = require('../../../utils/application-error')
const copyleaks = require("../../../libraries/copyleaks");
const ScanHistory = require('mongoose').models.ScanHistory;

module.exports = {
  method: 'get',
  path: '/api/v1/user/estimate-document-pages',
  handler: async function (req, res) {
    const content = await Content.findOne({_id: req.query.documentId});
    if (!content) {
      throw ApplicationErrors.NotFound();
    }
    const scan = new ScanHistory({
      ownerId: req.jwt.id,
      documentId: content._id,
      purpose: 'mint'
    });
    await scan.save();
    await copyleaks.estimateScan(content, scan);
  }
};
