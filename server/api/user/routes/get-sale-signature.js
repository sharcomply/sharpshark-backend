const ethSign = require('../../../blockchain/ethereum/signature');
const nearSign = require('../../../blockchain/near/signature');
const Content = require('mongoose').models.Content;
const BN = require('bn.js');
const expireToNanoSec = require("../../../utils/expire-to-nano-sec");

module.exports = {
  method: 'get',
  path: '/api/v1/user/get-sale-signature/:id',
  handler: async function (req, res) {
    const contentId = req.params.id;
    const amountWei = new BN(req.query.amountWei);
    const draft = await Content.findOne({_id: contentId, ownerId: req.jwt.id}).lean();
    if (!draft || !draft.isMinted) {
      return null;
    }
    const expireDate = new Date();
    expireDate.setDate(expireDate.getDate() + 1); // 24 hours expire
    const expire = Math.floor(expireDate.getTime() / 1000);

    const masterAmountBN = amountWei.mul(new BN(process.env.SALE_PLATFORM_FEE_PERCENT)).div(new BN(100));
    const minterAmountBN = amountWei.sub(masterAmountBN);

    const tokenId = draft.tokenId;
    const isEnabled = amountWei.gt(new BN(0));
    const amount = amountWei.toString(10);
    const minterAmount = minterAmountBN.toString(10);
    const masterAmount = masterAmountBN.toString(10);
    if (req.query.network === 'near') {
      const [signature, v] = await nearSign.signForSetSale(
        tokenId,
        isEnabled,
        amount,
        minterAmount,
        masterAmount,
        req.query.licenseHash,
        expire
      );

      return {
        token_id: tokenId,
        is_enabled: isEnabled,
        amount,
        minter_amount: minterAmount,
        master_amount: masterAmount,
        license_hash: req.query.licenseHash,
        expire: expireToNanoSec(expire),
        signature,
        v}
    } else {
      const signature = await ethSign.signForSetSale(
        tokenId,
        isEnabled,
        amount,
        minterAmount,
        masterAmount,
        req.query.licenseHash,
        expire
      );

      return {tokenId, isEnabled, amount, minterAmount, masterAmount, licenseHash: req.query.licenseHash, expire, signature}
    }
  }
};
