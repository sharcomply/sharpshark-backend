const Content = require('mongoose').models.Content;
const ApplicationErrors = require('../../../utils/application-error')
const BN = require("bn.js");
const ScanHistory = require('mongoose').models.ScanHistory;
const uploadToS3 = require('../../../utils/upload-to-s3');
const axios = require("axios");
const sanitizeHtml = require('../../../utils/sanitize-html');
const cache = require('../../../configs/cache');
const ethSign = require("../../../blockchain/ethereum/signature");
const nearSign = require("../../../blockchain/near/signature");
const estimateContentScanPrice = require('../../../utils/estimate-content-scan-price');
const convertFromUsd = require('../../../utils/convert-from-usd-to-wei');
const expireToNanoSec = require("../../../utils/expire-to-nano-sec");

module.exports = {
  method: 'get',
  path: '/api/v1/user/start-scan',
  handler: async function (req, res) {
    if (!(await cache.check())) {
      throw ApplicationErrors.MaxProtectionsError();
    }
    await cache.push();
    const queryParams = {
      _id: req.query.documentId,
      ownerId: req.jwt.id
    }
    const content = await Content.findOne(queryParams);
    if (!content) {
      throw ApplicationErrors.NotYouDocument();
    }
    if (content.type === 'text') {
      if (content.pagesCount === -1 ) {
        throw ApplicationErrors.NotFound();
      }
      const response = await axios.get(content.linkUrl);
      const sanitized = await sanitizeHtml(response.data);
      const filename = `${req.jwt.id}/${req.query.documentId}_sanitized.txt`;
      content.sanitizedLinkUrl = await uploadToS3(sanitized.sanitized, filename);
      content.additionalImages = sanitized.images;
      await content.save();
    }
    
    const newScan = new ScanHistory({
      ownerId: queryParams.ownerId,
      documentId: queryParams._id,
      purpose: 'mint',
      isResultIgnored: req.query.secret === process.env.SA_PASSWORD
    });
    await newScan.save();
    newScan.requestId = new BN(newScan._id.toString(), 16);
    await newScan.save();
    const requestId = newScan.requestId.toString(10);

    const expireDate = new Date();
    expireDate.setDate(expireDate.getDate() + 1); // 24 hours expire
    const expire = Math.floor(expireDate.getTime() / 1000);

    const priceInUsd = await estimateContentScanPrice(content);
    // todo: price in near
    const priceInWei = await convertFromUsd(priceInUsd, req.query.network ? req.query.network : 'eth');

    if (req.query.network === 'near') {
      const [signature, v] = await nearSign.signForRequest(
        requestId,
        priceInWei,
        expire
      );
      return {
        requestId,
        scanId: newScan._id,
        toBlockchain: {
          request_id: requestId,
          priceInWei,
          expire: expireToNanoSec(expire),
          signature,
          v
        }
      };
    } else {
      const signature = await ethSign.signForRequest(
        requestId,
        priceInWei,
        expire
      );
      return {
        requestId,
        scanId: newScan._id,
        toBlockchain: {
          requestId,
          priceInWei,
          expire,
          signature
        }
      };
    }
  }
};
