const Dispute = require('mongoose').models.Dispute;
const ApplicationError = require('../../../utils/application-error');
const sendMail = require("../../../mailing");

module.exports = {
  method: 'post',
  path: '/api/v1/user/send-emails-dispute/:id',
  handler: async function (req, res) {
    const dispute = await Dispute.findOne({_id: req.params.id, ownerId: req.jwt.id}).lean();
    if (!dispute) {
      throw ApplicationError.NotFound();
    }

    let emails = [];
    let text;
    if (dispute.status === 'In Progress: Website Owner') {
      emails = dispute.ownerClaimEmails;
      text = dispute.ownerClaimText;
    } else if (dispute.status === 'In Progress: Provider') {
      emails = dispute.providerClaimEmails;
      text = dispute.providerClaimText;
    } else {
      throw ApplicationError.DisputeStatusError();
    }
    if (emails.length === 0) {
      throw ApplicationError.DisputeEmailsError();
    }

    const cc = [];
    if (dispute.userContactEmail) {
      cc.push(dispute.userContactEmail);
    }
    for (let i = 0; i < emails.length; i++) {
      const emailObject = {
        email: emails[i],
        text: text,
        html: text,
        subject: 'Sharp Shark: Dispute',
      };
      await sendMail(emailObject, cc);
    }

    return {};
  }
};

