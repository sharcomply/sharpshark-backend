const User = require('mongoose').models.User;

module.exports = {
  method: 'post',
  path: '/api/v1/user/kyc-verification',
  handler: async function (req, res) {
    const user = await User.findOne({_id: req.jwt.id});
    user.email = req.body.email;
    user.country = req.body.country;
    user.language = req.body.language;
    await user.save();
    return user;
  }
};
