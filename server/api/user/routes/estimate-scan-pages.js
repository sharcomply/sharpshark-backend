const Content = require('mongoose').models.Content;
const ApplicationErrors = require('../../../utils/application-error')
const copyleaks = require("../../../libraries/copyleaks");
const ScanHistory = require('mongoose').models.ScanHistory;

module.exports = {
  method: 'get',
  path: '/api/v1/user/estimate-scan-pages',
  handler: async function (req, res) {
    const scan = await ScanHistory.findOne({requestId: req.query.requestId}).select('+nonceEstimate');
    if (!scan) {
      throw ApplicationErrors.NotFound();
    }
    const content = await Content.findOne({_id: scan.documentId});
    await copyleaks.estimateScan(content, scan);
  }
};
