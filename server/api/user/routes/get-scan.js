const ScanHistory = require('mongoose').models.ScanHistory;

module.exports = {
  method: 'get',
  path: '/api/v1/user/get-scans/:id',
  handler: async function (req, res) {
    const scanId = req.params.id;
    let scans = await ScanHistory.findOne({_id: scanId, ownerId: req.jwt.id}).lean();
    return scans;
  }
};
