const Alert = require('mongoose').models.Alert;

module.exports = {
  method: 'get',
  path: '/api/v1/user/get-alerts',
  handler: async function (req, res) {
    const queryParams = req.query;
    queryParams.ownerId = req.jwt.id;
    let items = await Alert.find(queryParams).populate('scanId').populate('documentId').lean();
    let count = await Alert.countDocuments(queryParams);
    await Alert.updateMany({_id: {$in: items.map(item => item._id)}}, {isRead: true});
    if (req.query.isFake) {
      try {
        const count = Number.parseInt(req.query.isFake);
        for (let i = 0; i < items.length; i++) {
          items[i].scanId.similarContent = Array(count).fill({
            "url": "http://example.com/",
            "title": "Example Domain",
            "introduction": "Example Domain This domain is for use in illustrative examples in documents. You may use this domain in literature without ...",
            "similarity": 25,
            "_id": "628f7ff49392b1fc394d6ea9"
          })
        }
      } catch (e) {
        // pass
      }
    }
    return {items, count}
  }
};
