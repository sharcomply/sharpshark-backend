const ObjectId = require('mongoose').Types.ObjectId;
const Dispute = require('mongoose').models.Dispute;
const pipelineForDocuments = require('../../../utils/pipeline-for-documents');
const pipelineForDocumentsAndAppeared = require('../../../utils/pipeline-for-documents-and-appeared');

module.exports = {
  method: 'get',
  path: '/api/v1/user/get-disputes-grouped',
  handler: async function (req, res) {
    const queryParams = req.query;
    queryParams.ownerId = new ObjectId(req.jwt.id);
    queryParams.isDeleted = {$ne: true};
    const paginationParams = {skip: req.query.skip ? Number.parseInt(req.query.skip) : 0, limit: req.query.limit ? Number.parseInt(req.query.limit) : 99999999};
    if (queryParams.status) {
      queryParams.status = {$in: queryParams.status.split(',')}
    }
    if (queryParams.documentId) {
      queryParams.documentId = new ObjectId(queryParams.documentId)
    }
    if (req.query.isNotRelevant) {
      queryParams.isNotRelevant = true;
    } else {
      queryParams.isNotRelevant = {$ne: !req.query.isNotRelevant};
    }
    let sortArray = [['createdAt', -1]]
    if (req.query.sort) {
      sortArray = JSON.parse(req.query.sort)
    }
    let sort = {};
    sortArray.forEach(arr => {
      sort[arr[0]] = arr[1];
    })

    let searchObj;
    if (req.query.search) {
      searchObj = {$match: {'documentId.title': { $regex: '.*' + decodeURIComponent(req.query.search) + '.*', $options : 'ui' }}}
    }
    const chosenPipeline = req.query.groupByAppearedOn === 'true' ? pipelineForDocumentsAndAppeared : pipelineForDocuments;

    delete queryParams.sort;
    delete queryParams.skip;
    delete queryParams.limit;
    delete queryParams.search;
    delete queryParams.groupByAppearedOn;

    const aggregatePipeline = [
      { $match: queryParams },
    ];


    let aggregatePipelineWithPaginationAndSorts = [...aggregatePipeline];
    aggregatePipelineWithPaginationAndSorts.push({$sort: sort});
    aggregatePipelineWithPaginationAndSorts = aggregatePipelineWithPaginationAndSorts.concat(chosenPipeline);
    if (searchObj) {
      aggregatePipelineWithPaginationAndSorts.push(searchObj)
    }
    aggregatePipelineWithPaginationAndSorts.push({$skip: paginationParams.skip});
    aggregatePipelineWithPaginationAndSorts.push({$limit: paginationParams.limit});

    const aggregatePipelineCount = [...aggregatePipeline].concat(chosenPipeline);
    if (searchObj) {
      aggregatePipelineCount.push(searchObj)
    }
    aggregatePipelineCount.push({$count: 'count'});

    let items = await Dispute.aggregate(aggregatePipelineWithPaginationAndSorts, {collation: {locale: 'en'}});
    let count = await Dispute.aggregate(aggregatePipelineCount);
    return {items, count: count[0]?.count ? count[0].count : 0}
  }
};
