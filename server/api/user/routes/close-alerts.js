const Alert = require('mongoose').models.Alert;

module.exports = {
  method: 'post',
  path: '/api/v1/user/close-alerts',
  handler: async function (req, res) {
    const ids = req.query.ids ? req.query.ids.split(',') : [];
    const {isReacted, isSkipped} = req.body;
    await Alert.updateMany({_id: {$in: ids}}, {isReacted, isSkipped});
    return {}
  }
};
