const Alert = require('mongoose').models.Alert;

module.exports = {
  method: 'get',
  path: '/api/v1/user/get-alert/:id',
  handler: async function (req, res) {
    const alert = await Alert.findOne({_id: req.params.id, ownerId: req.jwt.id}).populate('scanId').populate('documentId').lean();
    return alert;
  }
};
