const Content = require('mongoose').models.Content;

module.exports = {
  method: 'get',
  path: '/api/v1/user/get-drafts',
  handler: async function (req, res) {
    const queryParams = req.query;
    queryParams.ownerId = req.jwt.id;
    queryParams.isDeleted = {$ne: true};
    if (queryParams.isMinted === 'true') {
      queryParams.childId = {$exists: false};
      delete queryParams.isMinted;
      queryParams.$or = [
        {version: {$gt: 1}},
        {isMinted: true}
      ]
    }
    else {
      queryParams.parentId = {$exists: false};
    }
    let drafts = await Content.find(queryParams).lean();
    let count = await Content.countDocuments(queryParams);
    return {items: drafts, count}
  }
};
