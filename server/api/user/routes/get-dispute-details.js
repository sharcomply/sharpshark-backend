const Dispute = require('mongoose').models.Dispute;
const Alert = require('mongoose').models.Alert;

module.exports = {
  method: 'get',
  path: '/api/v1/user/get-dispute/:id',
  handler: async function (req, res) {
    const dispute = await Dispute.findOne({_id: req.params.id, ownerId: req.jwt.id}).populate('documentId').populate('ownerId').lean();
    dispute.alertId = await Alert.findOne({_id: dispute.alertId}).populate('scanId').lean();
    return dispute;
  }
};
