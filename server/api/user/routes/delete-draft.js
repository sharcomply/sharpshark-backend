const Content = require('mongoose').models.Content;

module.exports = {
  method: 'delete',
  path: '/api/v1/user/delete-draft/:id',
  handler: async function (req, res) {
    const paramsQuery = {
      _id: req.params.id,
      ownerId: req.jwt.id
    };
    await Content.updateOne(paramsQuery, {isDeleted: true, deleteDate: new Date()});
    return {}
  }
};
