const Content = require('mongoose').models.Content;
const { v4: uuidv4 } = require('uuid');
const ApplicationError = require('../../../utils/application-error');
const axios = require('axios');

module.exports = {
  method: 'post',
  path: '/api/v1/user/save-draft',
  handler: async function (req, res) {
    const userId =  req.jwt.id;
    const body = req.body;
    body.ownerId = userId;
    if (body.parentId) {
      const parent = await Content.findOne({_id: body.parentId, isMinted: true});
      if (!parent) {
        throw ApplicationError.ParentNotFoundError();
      }
      body.parentTokenId = parent.tokenId;
      body.version = parent.version + 1;
    } else {
      body.parentTokenId = null;
      body.version = 1;
    }
    if (!body.commonId) {
      body.commonId = uuidv4();
    }
    try {
      const newDraft = new Content(req.body);
      await newDraft.save();

      if (newDraft.parentId) {
        await Content.updateOne({_id: newDraft.parentId}, {childId: newDraft._id});
      }

      return newDraft;
    } catch (e) {
      console.log(e);
    }
  }
};
