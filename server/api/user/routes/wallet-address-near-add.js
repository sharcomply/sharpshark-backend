const User = require('mongoose').models.User;

module.exports = {
  method: 'post',
  path: '/api/v1/user/near-address',
  handler: async function (req, res) {
    const user = await User.findOne({_id: req.jwt.id});
    user.walletAddressNear = req.body.walletAddressNear;
    await user.save();
    return {walletAddressNear: user.walletAddressNear};
  }
};
