const Dispute = require('mongoose').models.Dispute;

module.exports = {
  method: 'patch',
  path: '/api/v1/user/update-status-dispute/:id',
  handler: async function (req, res) {
    const disputeId = req.params.id;
    const dispute = await Dispute.findOneAndUpdate({_id: disputeId}, {isOpen: false},
      {returnDocument: "after", returnNewDocument: true});
    return dispute;
  }
};
