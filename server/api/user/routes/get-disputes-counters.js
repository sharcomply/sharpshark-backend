const ObjectId = require('mongoose').Types.ObjectId;
const Dispute = require('mongoose').models.Dispute;
const pipelineForDocuments = require('../../../utils/pipeline-for-documents');
const pipelineForDocumentsAndAppeared = require('../../../utils/pipeline-for-documents-and-appeared');

module.exports = {
  method: 'get',
  path: '/api/v1/user/get-disputes-counters',
  handler: async function (req, res) {
    const queryParams = {ownerId: new ObjectId(req.jwt.id)};

    const aggregatePipeline = [
      { $match: queryParams },
      {
        $group: {
          _id: "$status",
          count: { $sum: 1 }
        }
      }
    ];

    let count = await Dispute.aggregate(aggregatePipeline);

    const result = {};
    for (let i = 0; i < count.length; i++) {
      result[count[i]._id] = count[i].count;
    }
    return result;
  }
};
