const Dispute = require('mongoose').models.Dispute;

module.exports = {
  method: 'delete',
  path: '/api/v1/user/delete-dispute/:id',
  handler: async function (req, res) {
    const paramsQuery = {
      _id: req.params.id,
      ownerId: req.jwt.id
    };
    await Dispute.updateOne(paramsQuery, {isDeleted: true, deleteDate: new Date()});
    return {}
  }
};
