const Content = require('mongoose').models.Content;
const ApplicationError = require('../../../utils/application-error');
const BN = require("bn.js");
const Monitoring = require('mongoose').models.Monitoring;
const ethSign = require("../../../blockchain/ethereum/signature");
const nearSign = require("../../../blockchain/near/signature");
const estimateContentScanPrice = require("../../../utils/estimate-content-scan-price");
const convertFromUsd = require("../../../utils/convert-from-usd-to-wei");
const expireToNanoSec = require("../../../utils/expire-to-nano-sec");

module.exports = {
  method: 'post',
  path: '/api/v1/user/setup-monitoring/:id',
  handler: async function (req, res) {
    const content = await Content.findOne({_id: req.params.id, ownerId: req.jwt.id});
    if (!content || !content.isMinted) {
      throw ApplicationError.NotFound();
    }
    const activeMonitoring = await Monitoring.find({documentId: content._id, isActive: true});
    if (activeMonitoring.length > 0) {
      throw ApplicationError.ActiveMonitoringsExists();
    }
    if (!req.body.scansTotal) {
      throw ApplicationError.ScansTotalError();
    }
    const monitoring = new Monitoring({
      documentId: content._id,
      scansTotal: req.body.scansTotal,
      isActive: false,
      isPayed: false,
      scansLeft: 0
    });
    await monitoring.save();
    monitoring.requestId = new BN(monitoring._id.toString(), 16);
    await monitoring.save();

    const requestId = monitoring.requestId.toString(10);

    const expireDate = new Date();
    expireDate.setDate(expireDate.getDate() + 1); // 24 hours expire
    const expire = Math.floor(expireDate.getTime() / 1000);

    const priceInUsd = await estimateContentScanPrice(content);
    const priceInWei = await convertFromUsd(priceInUsd, req.body.network ? req.body.network : 'eth', req.body.scansTotal);

    if (req.body.network === 'near') {
      const [signature, v] = await nearSign.signForMonitoringRequest(
        requestId,
        req.body.scansTotal,
        priceInWei,
        expire
      );
      return {
        requestId,
        toBlockchain: {
          request_id: requestId,
          scans_total: req.body.scansTotal,
          priceInWei,
          expire: expireToNanoSec(expire),
          signature,
          v
        }
      };
    } else {
      const signature = await ethSign.signForMonitoringRequest(
        requestId,
        req.body.scansTotal,
        priceInWei,
        expire
      );
      return {
        requestId,
        toBlockchain: {
          requestId,
          scansTotal: req.body.scansTotal,
          priceInWei,
          expire,
          signature
        }
      };
    }
  }
};
