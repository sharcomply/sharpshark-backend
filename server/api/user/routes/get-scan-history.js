const ScanHistory = require('mongoose').models.ScanHistory;

module.exports = {
  method: 'get',
  path: '/api/v1/user/get-scan-history',
  handler: async function (req, res) {
    const userId = req.jwt.id;
    const queryParams = {ownerId: userId};
    let scans = await ScanHistory.find(queryParams);
    let count = await ScanHistory.countDocuments(queryParams);
    return {items: scans, count};
  }
};
