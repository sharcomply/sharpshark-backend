const User = require('mongoose').models.User;
const Promise = require('bluebird');
const Dispute = require('mongoose').models.Dispute;
const Content = require('mongoose').models.Content;
const Alert = require('mongoose').models.Alert;

module.exports = {
  method: 'get',
  path: '/api/v1/user/whoami',
  handler: async function (req, res) {
    const websocket = `${process.env.WS_PROTOCOL}://${process.env.SERVER_IP}/ws?token=${req.header('Authorization')}`;
    const [result, drafts, protects, alerts, disputes, unreadAlerts] = await Promise.all([
      User.findOne({_id: req.jwt.id}),
      Content.countDocuments({
        ownerId: req.jwt.id,
        isMinted: false,
        parentId: {$exists: false},
        isDeleted: {$ne: true}
      }),
      Content.countDocuments({
        ownerId: req.jwt.id,
        $or: [
          {version: {$gt: 1}},
          {isMinted: true}
        ],
        isDeleted: {$ne: true},
        childId: {$exists: false},
      }),
      Alert.countDocuments({ownerId: req.jwt.id}),
      Dispute.countDocuments({ownerId: req.jwt.id}),
      Alert.countDocuments({ownerId: req.jwt.id, isRead: false}),
    ]);
    const counts = {drafts, protects, alerts, disputes, unreadAlerts};
    return {data: result, websocket: websocket, counts};
  }
};
