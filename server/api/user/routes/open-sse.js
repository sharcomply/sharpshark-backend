const connectedSockets = require('../../../sse/connected-sockets')

module.exports = {
  method: 'get',
  path: '/api/v1/user/open-sse',
  handler: async function (req, res) {
    let userId = req.jwt.id;
    connectedSockets[userId] = {res: res};
    req.on('close', () => {
      delete connectedSockets[userId];
    });
  
    res.writeHead(200, {
      Connection: 'keep-alive',
      'Content-Type': 'text/event-stream',
      'Cache-Control': 'no-cache'
    });
    return {}
  }
};
