const Dispute = require('mongoose').models.Dispute;

module.exports = {
  method: 'get',
  path: '/api/v1/user/get-disputes',
  handler: async function (req, res) {
    const queryParams = req.query;
    queryParams.ownerId = req.jwt.id;
    queryParams.isDeleted = {$ne: true};
    if (queryParams.status) {
      queryParams.status = {$in: queryParams.status.split(',')}
    }

    if (req.query.isNotRelevant) {
      queryParams.isNotRelevant = true;
    } else {
      queryParams.isNotRelevant = {$ne: !req.query.isNotRelevant};
    }

    let items = await Dispute.find(queryParams).populate('documentId').populate({path: 'alertId', populate: {path: 'scanId'}}).lean();
    let count = await Dispute.countDocuments(queryParams);
    return {items, count}
  }
};
