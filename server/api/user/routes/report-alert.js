const Alert = require('mongoose').models.Alert;

module.exports = {
  method: 'post',
  path: '/api/v1/user/report-alert/:id',
  handler: async function (req, res) {
    await Alert.updateOne({_id: req.params.id}, {report: req.body.report});
    return {}
  }
};
