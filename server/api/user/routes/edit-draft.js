const axios = require("axios");
const Content = require('mongoose').models.Content;
const ApplicationError = require('../../../utils/application-error');

module.exports = {
  method: 'patch',
  path: '/api/v1/user/edit-draft/:id',
  handler: async function (req, res) {
    const paramsQuery = {
      _id: req.params.id,
      ownerId: req.jwt.id,
      isMinted: false
    };
    if (req.body.linkUrl) {
      const response = await axios.head(req.body.linkUrl);
      if (Number.parseInt(response.headers['content-length']) < 50) {
        throw ApplicationError.ContentLengthTooLowError();
      }
    }
    const updateDraft = await Content.findOneAndUpdate(paramsQuery, req.body,
      {returnDocument: "after", returnNewDocument: true});
    return updateDraft;
  }
};
