const sendMail = require('../../../mailing');
const templates = require('../../../mailing/templates');
const jwt = require('jsonwebtoken');
const User = require('mongoose').models.User;

module.exports = {
  method: 'post',
  path: '/api/v1/user/email',
  handler: async function (req, res) {
    const user = await User.findOne({_id: req.jwt.id});
    user.email = req.body.email;
    user.isEmailVerified = false;

    const token = jwt.sign({
      email: user.email,
      _id: user._id,
    }, process.env.JWT_SECRET, {expiresIn: Number.parseInt(process.env.MAIL_VERIFY_VALID_SECONDS)});
    const link = `https://${process.env.SERVER_IP}/auth/verify?token=${token}`;
    const emailObject = {
      email: user.email,
      text: templates.verify.text,
      html: templates.verify.html,
      subject: templates.verify.subject,
      link
    };

    sendMail(emailObject);
    await user.save();
    return {isEmailVerified: false, email: user.email};
  }
};
