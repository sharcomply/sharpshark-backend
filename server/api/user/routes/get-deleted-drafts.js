const Content = require('mongoose').models.Content;

module.exports = {
  method: 'get',
  path: '/api/v1/user/get-deleted-drafts',
  handler: async function (req, res) {
    const queryParams = req.query;
    queryParams.ownerId = req.jwt.id;
    queryParams.isDeleted = true;

    const thirtyDaysAgo = new Date();
    thirtyDaysAgo.setDate(thirtyDaysAgo.getDate()-30);
    queryParams.deleteDate = {$gt: thirtyDaysAgo};

    let drafts = await Content.find(queryParams).lean();
    let count = await Content.countDocuments(queryParams);
    return {items: drafts, count}
  }
};
