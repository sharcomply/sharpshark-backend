const Content = require('mongoose').models.Content;
const Monitoring = require('mongoose').models.Monitoring;

module.exports = {
  method: 'get',
  path: '/api/v1/user/get-drafts/:id',
  handler: async function (req, res) {
    const contentId = req.params.id;
    const draft = await Content.findOne({_id: contentId, ownerId: req.jwt.id}).lean();
    if (!draft) {
      return draft;
    }
    if (draft) {
      draft.activeMonitor = await Monitoring.findOne({isActive: true, documentId: contentId});
    }
    draft.versionsHistory = {};
    draft.versionsHistoryObjects = {};
    draft.versionsHistory[draft.version] = draft._id;
    draft.versionsHistoryObjects[draft.version] = await Content.findOne({_id: draft._id}).lean();
    let childId = draft.childId;
    let parentId = draft.parentId;
    while (childId) {
      const newDraft = await Content.findOne({_id: childId}).lean();
      draft.versionsHistory[newDraft.version] = newDraft._id;
      draft.versionsHistoryObjects[newDraft.version] = newDraft;
      childId = newDraft.childId;
    }
    while (parentId) {
      const newDraft = await Content.findOne({_id: parentId}).lean();
      draft.versionsHistory[newDraft.version] = newDraft._id;
      draft.versionsHistoryObjects[newDraft.version] = newDraft;
      parentId = newDraft.parentId;
    }
    return draft;
  }
};