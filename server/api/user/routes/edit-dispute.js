const Dispute = require('mongoose').models.Dispute;
const User = require('mongoose').models.User;
const ApplicationError = require('../../../utils/application-error');

module.exports = {
  method: 'post',
  path: '/api/v1/user/edit-dispute/:id',
  handler: async function (req, res) {
    delete req.body.alertId;
    delete req.body.contentId;
    delete req.body.discoveredDate;
    delete req.body.ownerId;
    delete req.body.appearedOn;

    // if (req.body.status && ![
    //   'Draft: Website Owner',
    //   'In Progress: Website Owner',
    //   'Draft: Provider',
    //   'In Progress: Provider',
    //   'Draft: Google',
    //   'In Progress: Google',
    //   'Closed',
    //   'Under Court'
    // ].includes(req.body.status)) {
    //   throw ApplicationError.BadRequest();
    // }
    if (req.body.userContactEmail) {
      await User.findOneAndUpdate({_id: req.jwt.id,}, {disputesPrefillEmail: req.body.userContactEmail},
        {returnDocument: "after", returnNewDocument: true})
    }

    const paramsQuery = {
      _id: req.params.id,
      ownerId: req.jwt.id,
    };
    return Dispute.findOneAndUpdate(paramsQuery, req.body,
      {returnDocument: "after", returnNewDocument: true});
  }
};

