const Dispute = require('mongoose').models.Dispute;
const Alert = require('mongoose').models.Alert
const ApplicationError = require('../../../utils/application-error');

module.exports = {
  method: 'post',
  path: '/api/v1/user/save-dispute',
  handler: async function (req, res) {
    const alert = await Alert.findOne({_id: req.body.alertId});
    if (!alert) {
      throw ApplicationError.NotFound();
    }
    if (req.body.status && !['Draft: Website Owner', 'In Progress: Website Owner'].includes(req.body.status)) {
      throw ApplicationError.BadRequest();
    }

    req.body.ownerId = req.jwt.id;
    req.body.documentId = alert.documentId;
    req.body.discoveredDate = alert.date;

    const dispute = new Dispute(req.body);
    await dispute.save();
    return dispute;
  }
};