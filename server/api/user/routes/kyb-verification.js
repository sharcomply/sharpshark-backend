const SessionShuftipro = require('mongoose').models.SessionShuftipro;
const axios = require('axios');
const sessionToken = require("../../../utils/access-token-shuftipro");
const ApplicationError = require('../../../utils/application-error');
const connections = require("../../../websocket/event-subscribers/connected-sockets");

module.exports = {
  method: 'post',
  path: '/api/v1/user/kyb-verification',
  handler: async function (req, res) {
    /*if (req.jwt.isVerification) {
      throw ApplicationError.VerificationAlreadyExists();
    }*/
    const body = req.body;
    if (!body.company_name) {
      throw ApplicationError.BadRequest();
    }
    const newSession = new SessionShuftipro({
      userId: req.jwt.id
    });
    await newSession.save();
    const session = await sessionToken(req);
    const result = await axios.post('https://api.shuftipro.com/',
      {
        reference         : `${newSession.generatedUserId}`,
        callback_url      : process.env.HTTP_PROTOCOL+'://'+ process.env.SERVER_IP+process.env.KYB_CALLBACK,
        country           : body.country? body.country : '',
        language          : body.language? body.language : '',
        kyb: {
          company_name: body.company_name,
        }
      },
      {
        headers:
          {
            'Accept'        : 'application/json',
            'Content-Type'  : 'application/json',
            'Authorization' : 'Bearer '+session.accessToken
          }
      }
    );
    if(result.data["verification_data"]["kyb"].length > 0) {
      return {isVerification: true, data: result.data["verification_data"]["kyb"]};
    } else {
      return {isVerification: false,data: result.data};
    }
  }
};
