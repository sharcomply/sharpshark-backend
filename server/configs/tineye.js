const TinEye = require('tineye-api')
const api = new TinEye(
  'https://api.tineye.com/rest/',
  process.env.TINEYE_API_KEY
);
module.exports = api;