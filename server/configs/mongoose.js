const mongoose = require('mongoose');
const glob = require('glob');
// Create the database connection
mongoose.connect(process.env.MONGOOSE_URL, {
  useNewUrlParser: true,
  useUnifiedTopology : true
});

// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on('connected', function () {
  console.log('Mongoose default connection open to ' + process.env.MONGOOSE_URL);
});

// If the connection throws an error
mongoose.connection.on('error', function (err) {
  console.log('Mongoose default connection error: ' + err);
  mongoose.disconnect();
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
  console.log('Mongoose default connection disconnected');
});

const definedModels = glob.sync('server/dao/mongoose-models/*.js');

definedModels.forEach((path) => {
    require(`../dao/mongoose-models/${path.split('/')[ 3 ]}`);
  }
);
