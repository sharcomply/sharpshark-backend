const cacheManager = require('cache-manager');
const memoryCache = cacheManager.caching({});
const uuid = require('uuid');

exports.push = async () => {
  await memoryCache.set(uuid.v4(), null, {ttl: 60});
}
exports.check = async () => {
  const c = await memoryCache.store.keys();
  return c.length < 10;
}