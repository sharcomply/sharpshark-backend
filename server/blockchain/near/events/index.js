const fs = require('fs');
const path = require('path');
const ws = require('ws');

const handlers = {};
const basename = path.basename(__filename);
fs.readdirSync(__dirname).filter(file => {
  return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
}).forEach(file => {
  handlers[file.split('.')[0]] = require(`${__dirname}/${file}`);
});

const client = new ws.WebSocket(process.env.NEAR_EVENTS_WEBSOCKET);
client.onmessage = (msg) => {
  const msgJson = JSON.parse(msg.data);
  if (msgJson.action === 'ping') {
    return;
  }
  if (msgJson.length) {
    const obj = msgJson[0];
    const transactionHash = obj.receipt.id;
    if (!obj.events.length) {
      return;
    }
    const event = obj.events[0];
    handlers[event.event](transactionHash, event.data[0]);
  }
};
