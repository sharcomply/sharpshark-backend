const copyleaks = require("../../../libraries/copyleaks");
const ScanHistory = require('mongoose').models.ScanHistory;
const Content = require('mongoose').models.Content;
const connections = require('../../../websocket/event-subscribers/connected-sockets');
const tineyeScan = require('../../../utils/tineye-scan');
const network = 'near';

module.exports = async (transactionHash, event) => {
  const requestId = event.request_id;

  console.log(`${network}: REQUEST FOR ANTIPLUG PAYED EVENT: requestId=${requestId}, txHash=${transactionHash}.`);

  const scan = await ScanHistory.findOne({requestId}).select('+nonce');
  if (!scan) {
    return;
  }
  scan.isRequestPayed = true;
  await scan.save();

  const content = await Content.findOne({_id: scan.documentId});
  const connection = connections[scan.ownerId];
  if (connection) {
    connection.send(JSON.stringify({
      action: 'scanCurrentStatus', // 1
      status: 'paymentReceived',
      scanId: scan._id,
      draftId: content._id,
    }));
    console.log(`Sent paymentReceived for uniqueId=${connection.uniqueId}`)
  } else {
    console.log(`No connection for user ${scan.ownerId}`)
  }

  if (content.type === 'image') {
    await tineyeScan(content, scan, 'mint', network);
  } else {
    await copyleaks.submitScan(content, scan, network);
  }
}