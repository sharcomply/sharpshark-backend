const crypto = require('crypto')
const secp256k1 = require('secp256k1');
const expireToNanoSec = require('../../../utils/expire-to-nano-sec');
const fromHexString = (hexString) => Uint8Array.from(hexString.match(/.{1,2}/g).map((byte) => parseInt(byte, 16)));

function sign(obj) {
  const privateKeyString = process.env.SIGNATURE_PRIVATE_KEY.substring(2);
  const privateKey = fromHexString(privateKeyString);
  const msg = JSON.stringify(obj);
  const msgHash = Uint8Array.from(crypto.createHash('sha256').update(msg).digest());
  const sigObj = secp256k1.ecdsaSign(msgHash, privateKey)
  const sigArray = Array.from(sigObj.signature)
  const v = sigObj.recid;
  return [sigArray, v];
}

exports.signForMint = async (
  tokenId,
  to,
  contentHash,
  metaDataHash,
  expire,
  parentTokenId,
  version
) => {
  return sign({
    tokenId,
    receiverId: to,
    contentHash,
    metadataHash: metaDataHash,
    expire: expireToNanoSec(expire),
    parentTokenId,
    version,
  });
};

exports.signForSetSale = async (
  tokenId,
  isEnabled,
  amount,
  minterAmount,
  masterAmount,
  licenseHash,
  expire,
) => {
  return sign({
    tokenId,
    isEnabled,
    amount,
    minterAmount,
    masterAmount,
    licenseHash,
    expire: expireToNanoSec(expire),
  });
}

exports.signForRequest = async (
  requestId,
  price,
  expire,
) => {
  return sign({
    requestId,
    value: price,
    expire: expireToNanoSec(expire),
  });
}

exports.signForMonitoringRequest = async (
  requestId,
  scansTotal,
  price,
  expire,
) => {
  return sign({
    requestId,
    scansTotal,
    value: price,
    expire: expireToNanoSec(expire),
  });
}