const ethUtil = require('ethereumjs-util');
const ethAbi = require('ethereumjs-abi');

exports.signForMint = async (
  tokenId,
  to,
  contentHash,
  metaDataHash,
  expire,
  parentTokenId,
  version
) => {
  const privateKey = ethUtil.toBuffer(process.env.SIGNATURE_PRIVATE_KEY);
  const hashedMessage = ethUtil.keccak(
    ethAbi.rawEncode([
        'uint',
        'address',
        'string',
        'string',
        'uint',
        'uint',
        'uint',
      ],
      [
        tokenId,
        to,
        contentHash,
        metaDataHash,
        expire,
        parentTokenId,
        version
      ]));
  const signature = ethUtil.ecsign(hashedMessage, privateKey);
  return ethUtil.toRpcSig(signature.v, signature.r, signature.s).toString("hex");
};

exports.signForSetSale = async (
  tokenId,
  isEnabled,
  amount,
  minterAmount,
  masterAmount,
  licenseHash,
  expire,
) => {
  const privateKey = ethUtil.toBuffer(process.env.SIGNATURE_PRIVATE_KEY);
  const hashedMessage = ethUtil.keccak(
    ethAbi.rawEncode([
        'uint',
        'bool',
        'uint',
        'uint',
        'uint',
        'string',
        'uint',
      ],
      [
        tokenId,
        isEnabled,
        amount,
        minterAmount,
        masterAmount,
        licenseHash,
        expire,
      ]));
  const signature = ethUtil.ecsign(hashedMessage, privateKey);
  return ethUtil.toRpcSig(signature.v, signature.r, signature.s).toString("hex");
}

exports.signForRequest = async (
  requestId,
  price,
  expire,
) => {
  const privateKey = ethUtil.toBuffer(process.env.SIGNATURE_PRIVATE_KEY);
  const hashedMessage = ethUtil.keccak(
    ethAbi.rawEncode([
        'uint',
        'uint',
        'uint',
      ],
      [
        requestId,
        price,
        expire,
      ]));
  const signature = ethUtil.ecsign(hashedMessage, privateKey);
  return ethUtil.toRpcSig(signature.v, signature.r, signature.s).toString("hex");
}

exports.signForMonitoringRequest = async (
  requestId,
  scansTotal,
  price,
  expire,
) => {
  const privateKey = ethUtil.toBuffer(process.env.SIGNATURE_PRIVATE_KEY);
  const hashedMessage = ethUtil.keccak(
    ethAbi.rawEncode([
        'uint',
        'uint',
        'uint',
        'uint',
      ],
      [
        requestId,
        scansTotal,
        price,
        expire,
      ]));
  const signature = ethUtil.ecsign(hashedMessage, privateKey);
  return ethUtil.toRpcSig(signature.v, signature.r, signature.s).toString("hex");
}