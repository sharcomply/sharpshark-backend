module.exports = [{
  "inputs": [{"internalType": "string", "name": "name_", "type": "string"}, {
    "internalType": "string",
    "name": "symbol_",
    "type": "string"
  }, {"internalType": "address", "name": "publicKey_", "type": "address"}, {
    "internalType": "string",
    "name": "baseURI_",
    "type": "string"
  }], "stateMutability": "nonpayable", "type": "constructor"
}, {
  "anonymous": false,
  "inputs": [{"indexed": true, "internalType": "address", "name": "owner", "type": "address"}, {
    "indexed": true,
    "internalType": "address",
    "name": "approved",
    "type": "address"
  }, {"indexed": true, "internalType": "uint256", "name": "tokenId", "type": "uint256"}],
  "name": "Approval",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{"indexed": true, "internalType": "address", "name": "owner", "type": "address"}, {
    "indexed": true,
    "internalType": "address",
    "name": "operator",
    "type": "address"
  }, {"indexed": false, "internalType": "bool", "name": "approved", "type": "bool"}],
  "name": "ApprovalForAll",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{"indexed": false, "internalType": "address", "name": "to", "type": "address"}, {
    "indexed": false,
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }, {"indexed": false, "internalType": "bool", "name": "isSubToken", "type": "bool"}],
  "name": "Mint",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{"indexed": false, "internalType": "uint256", "name": "requestId", "type": "uint256"}, {
    "indexed": false,
    "internalType": "uint256",
    "name": "scansTotal",
    "type": "uint256"
  }],
  "name": "MonitoringRequestPayed",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{"indexed": true, "internalType": "address", "name": "previousOwner", "type": "address"}, {
    "indexed": true,
    "internalType": "address",
    "name": "newOwner",
    "type": "address"
  }],
  "name": "OwnershipTransferred",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{"indexed": false, "internalType": "uint256", "name": "requestId", "type": "uint256"}],
  "name": "RequestPayed",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{"indexed": true, "internalType": "address", "name": "from", "type": "address"}, {
    "indexed": true,
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {"indexed": true, "internalType": "uint256", "name": "tokenId", "type": "uint256"}],
  "name": "Transfer",
  "type": "event"
}, {
  "inputs": [{"internalType": "address", "name": "to", "type": "address"}, {
    "internalType": "uint256",
    "name": "tokenId",
    "type": "uint256"
  }], "name": "approve", "outputs": [], "stateMutability": "nonpayable", "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "owner", "type": "address"}],
  "name": "balanceOf",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "tokenId", "type": "uint256"}],
  "name": "buyCert",
  "outputs": [],
  "stateMutability": "payable",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "tokenId", "type": "uint256"}],
  "name": "contentHash",
  "outputs": [{"internalType": "string", "name": "", "type": "string"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "tokenId", "type": "uint256"}],
  "name": "createdDate",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "tokenId", "type": "uint256"}],
  "name": "currentOwner",
  "outputs": [{"internalType": "address", "name": "", "type": "address"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "tokenId", "type": "uint256"}],
  "name": "expiredDate",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "tokenId", "type": "uint256"}],
  "name": "getApproved",
  "outputs": [{"internalType": "address", "name": "", "type": "address"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "requestId", "type": "uint256"}],
  "name": "getRequestPayed",
  "outputs": [{"internalType": "bool", "name": "", "type": "bool"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "tokenId", "type": "uint256"}],
  "name": "getSubTokenInfo",
  "outputs": [{
    "components": [{
      "internalType": "uint256",
      "name": "createdDate",
      "type": "uint256"
    }, {"internalType": "uint256", "name": "expiredDate", "type": "uint256"}, {
      "internalType": "uint256",
      "name": "parentTokenId",
      "type": "uint256"
    }, {"internalType": "string", "name": "licenseHash", "type": "string"}, {
      "internalType": "address",
      "name": "minter",
      "type": "address"
    }, {"internalType": "bool", "name": "isExists", "type": "bool"}],
    "internalType": "struct SharkKeeper.SubTokenInfo",
    "name": "",
    "type": "tuple"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "tokenId", "type": "uint256"}],
  "name": "getTokenInfo",
  "outputs": [{
    "components": [{
      "internalType": "uint256",
      "name": "saleAmount",
      "type": "uint256"
    }, {"internalType": "uint256", "name": "saleMinterAmount", "type": "uint256"}, {
      "internalType": "uint256",
      "name": "saleMasterAmount",
      "type": "uint256"
    }, {"internalType": "uint256", "name": "parentTokenId", "type": "uint256"}, {
      "internalType": "uint256",
      "name": "version",
      "type": "uint256"
    }, {"internalType": "string", "name": "metadataHash", "type": "string"}, {
      "internalType": "string",
      "name": "contentHash",
      "type": "string"
    }, {"internalType": "string", "name": "licenseHash", "type": "string"}, {
      "internalType": "address",
      "name": "minter",
      "type": "address"
    }, {"internalType": "bool", "name": "saleIsEnabled", "type": "bool"}, {
      "internalType": "uint256",
      "name": "createdDate",
      "type": "uint256"
    }, {"internalType": "bool", "name": "isExists", "type": "bool"}],
    "internalType": "struct SharkKeeper.TokenInfo",
    "name": "",
    "type": "tuple"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "getTransfersAllowed",
  "outputs": [{"internalType": "bool", "name": "", "type": "bool"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "owner", "type": "address"}, {
    "internalType": "address",
    "name": "operator",
    "type": "address"
  }],
  "name": "isApprovedForAll",
  "outputs": [{"internalType": "bool", "name": "", "type": "bool"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "tokenId", "type": "uint256"}],
  "name": "licenseHash",
  "outputs": [{"internalType": "string", "name": "", "type": "string"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "tokenId", "type": "uint256"}],
  "name": "metaDataHash",
  "outputs": [{"internalType": "string", "name": "", "type": "string"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "tokenId", "type": "uint256"}, {
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {"internalType": "string", "name": "contentHash_", "type": "string"}, {
    "internalType": "string",
    "name": "metaDataHash_",
    "type": "string"
  }, {"internalType": "uint256", "name": "expire", "type": "uint256"}, {
    "internalType": "bytes",
    "name": "signature",
    "type": "bytes"
  }, {"internalType": "uint256", "name": "parentTokenId", "type": "uint256"}, {
    "internalType": "uint256",
    "name": "version",
    "type": "uint256"
  }], "name": "mint", "outputs": [], "stateMutability": "nonpayable", "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "tokenId", "type": "uint256"}],
  "name": "minter",
  "outputs": [{"internalType": "address", "name": "", "type": "address"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "name",
  "outputs": [{"internalType": "string", "name": "", "type": "string"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "owner",
  "outputs": [{"internalType": "address", "name": "", "type": "address"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "tokenId", "type": "uint256"}],
  "name": "ownerOf",
  "outputs": [{"internalType": "address", "name": "", "type": "address"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "tokenId", "type": "uint256"}],
  "name": "parentTokenId",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "requestId", "type": "uint256"}, {
    "internalType": "uint256",
    "name": "scansTotal",
    "type": "uint256"
  }, {"internalType": "uint256", "name": "expire", "type": "uint256"}, {
    "internalType": "bytes",
    "name": "signature",
    "type": "bytes"
  }], "name": "payMonitoringRequest", "outputs": [], "stateMutability": "payable", "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "requestId", "type": "uint256"}, {
    "internalType": "uint256",
    "name": "expire",
    "type": "uint256"
  }, {"internalType": "bytes", "name": "signature", "type": "bytes"}],
  "name": "payRequest",
  "outputs": [],
  "stateMutability": "payable",
  "type": "function"
}, {
  "inputs": [],
  "name": "renounceOwnership",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "from", "type": "address"}, {
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {"internalType": "uint256", "name": "tokenId", "type": "uint256"}],
  "name": "safeTransferFrom",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "from", "type": "address"}, {
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {"internalType": "uint256", "name": "tokenId", "type": "uint256"}, {
    "internalType": "bytes",
    "name": "_data",
    "type": "bytes"
  }], "name": "safeTransferFrom", "outputs": [], "stateMutability": "nonpayable", "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "operator", "type": "address"}, {
    "internalType": "bool",
    "name": "approved",
    "type": "bool"
  }], "name": "setApprovalForAll", "outputs": [], "stateMutability": "nonpayable", "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "tokenId", "type": "uint256"}, {
    "internalType": "string",
    "name": "licenseHash_",
    "type": "string"
  }], "name": "setLicenseHash", "outputs": [], "stateMutability": "nonpayable", "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "publicKey_", "type": "address"}],
  "name": "setPublicKey",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "tokenId", "type": "uint256"}, {
    "internalType": "bool",
    "name": "isEnabled",
    "type": "bool"
  }, {"internalType": "uint256", "name": "amount", "type": "uint256"}, {
    "internalType": "uint256",
    "name": "minterAmount",
    "type": "uint256"
  }, {"internalType": "uint256", "name": "masterAmount", "type": "uint256"}, {
    "internalType": "string",
    "name": "licenseHash_",
    "type": "string"
  }, {"internalType": "uint256", "name": "expire", "type": "uint256"}, {
    "internalType": "bytes",
    "name": "signature",
    "type": "bytes"
  }], "name": "setSale", "outputs": [], "stateMutability": "nonpayable", "type": "function"
}, {
  "inputs": [{"internalType": "bool", "name": "canTransfer", "type": "bool"}],
  "name": "setTransfersAllowed",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{"internalType": "bytes4", "name": "interfaceId", "type": "bytes4"}],
  "name": "supportsInterface",
  "outputs": [{"internalType": "bool", "name": "", "type": "bool"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "symbol",
  "outputs": [{"internalType": "string", "name": "", "type": "string"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "tokenId", "type": "uint256"}],
  "name": "tokenURI",
  "outputs": [{"internalType": "string", "name": "", "type": "string"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "from", "type": "address"}, {
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {"internalType": "uint256", "name": "tokenId", "type": "uint256"}],
  "name": "transferFrom",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "newOwner", "type": "address"}],
  "name": "transferOwnership",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "tokenId", "type": "uint256"}],
  "name": "version",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}];