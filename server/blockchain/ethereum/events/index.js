const { ethers } = require("ethers");
const abi = require('../abi');
const fs = require('fs');
const path = require('path');

const handlers = {};
const basename = path.basename(__filename);
fs.readdirSync(__dirname).filter(file => {
  return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
}).forEach(file => {
  handlers[file.split('.')[0]] = require(`${__dirname}/${file}`);
});

const EXPECTED_PONG_BACK = 15000
const KEEP_ALIVE_CHECK_INTERVAL = 7500
const RETRY_INTERVAL = 60000;

const providers = {};
const contracts = {};
const retryRequests = {};

function startConnection(network) {
  switch (network) {
    case 'bsc':
      providers[network] =  new ethers.providers.WebSocketProvider(process.env.BSC_WS_URL, Number.parseInt(process.env.BSC_NETWORK));
      break;
    case 'eth':
      providers[network] =  new ethers.providers.WebSocketProvider(process.env.ETHEREUM_WS_URL, Number.parseInt(process.env.ETHEREUM_NETWORK));
      break;
    case 'polygon':
      providers[network] =  new ethers.providers.WebSocketProvider(process.env.POLYGON_WS_URL, Number.parseInt(process.env.POLYGON_NETWORK));
      break;
  }

  let pingTimeout = null
  let keepAliveInterval = null
  let retryInterval = null

  providers[network]._websocket.on('open', () => {
    keepAliveInterval = setInterval(() => {
      // console.log('Checking if the connection is alive, sending a ping')
      providers[network]._websocket.ping()
      pingTimeout = setTimeout(() => {
        providers[network]._websocket.terminate()
      }, EXPECTED_PONG_BACK)
    }, KEEP_ALIVE_CHECK_INTERVAL)

    switch(network) {
      case 'bsc':
        contracts[network] = new ethers.Contract(process.env.BSC_CONTRACT_ADDRESS, abi, providers[network]);
        break;
      case 'eth':
        contracts[network] = new ethers.Contract(process.env.ETHEREUM_CONTRACT_ADDRESS, abi, providers[network]);
        break;
      case 'polygon':
        contracts[network] = new ethers.Contract(process.env.POLYGON_CONTRACT_ADDRESS, abi, providers[network]);
        break;
    }
    for (let handler in handlers) {
      contracts[network].on(handler, handlers[handler](network));
    }
    console.log(`Contract listeners set up for ${network}`);
  })

  retryInterval = setInterval(() => {
    if (retryRequests[network]) {
      console.log(`Network ${network} was closed. Retrying connection...`);
      startConnection(network)
      retryRequests[network] = false;
    }
  }, RETRY_INTERVAL);
  async function retryRequest() {
    retryRequests[network] = true;
    clearInterval(keepAliveInterval)
    clearTimeout(pingTimeout)
  }

  providers[network]._websocket.on('close', retryRequest)

  providers[network]._websocket.on('error', retryRequest)

  providers[network]._websocket.on('pong', () => {
    // console.log('Received pong, so connection is alive, clearing the timeout')
    clearInterval(pingTimeout)
  })
}
startConnection('bsc');
startConnection('eth');
startConnection('polygon');
module.exports = contracts;
