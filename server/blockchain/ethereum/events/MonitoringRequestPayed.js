const Monitoring = require('mongoose').models.Monitoring;
const connections = require('../../../websocket/event-subscribers/connected-sockets');

module.exports = (network) =>
  async (requestId, scansCount, event) => {
    console.log(`${network}: REQUEST FOR MONITORING PAYED EVENT: requestId=${requestId}, scansCount=${scansCount}, txHash=${event.transactionHash}.`);

    const monitoring = await Monitoring.findOne({requestId}).populate('documentId');
    monitoring.isActive = scansCount > 0;
    monitoring.isPayed = true;
    monitoring.scansLeft = scansCount;
    await monitoring.save();

    const connection = connections[monitoring.documentId.ownerId];
    if (connection) {
      connection.send(JSON.stringify({
        action: 'monitoringStatus', // 1
        status: 'paymentReceived',
        draftId: monitoring.documentId._id,
      }));
      console.log(`Sent paymentReceived for uniqueId=${connection.uniqueId}`)
    } else {
      console.log(`No connection for user ${monitoring.documentId.ownerId}`)
    }
  }