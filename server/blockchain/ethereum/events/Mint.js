const connections = require("../../../websocket/event-subscribers/connected-sockets");
const Content = require('mongoose').models.Content;
const Mint = require('mongoose').models.Mint;
const User = require('mongoose').models.User;
const generateAndUploadQr = require('../../../utils/generate-and-upload-qr');
const Promise = require('bluebird');
const mintContentNetwork = require('../../../utils/mint-content-network');

module.exports = (network) =>
  async (to, tokenId, isSubtoken, event) => {
    if (isSubtoken) {
      console.log(`${network}: SUBTOKEN MINT EVENT: to=${to}, id=${tokenId}, txHash=${event.transactionHash}.`);
    } else {
      console.log(`${network}: MINT EVENT: to=${to}, id=${tokenId}, txHash=${event.transactionHash}.`);

      const content = await Content.findOne({tokenId: tokenId});
      content.isMinted = true;

      await mintContentNetwork(content, network, event.transactionHash);

      await content.save();

      const connection = connections[content.ownerId];
      if (connection) {
        connection.send(JSON.stringify({
          action: 'scanCurrentStatus', // 5
          status: 'minted',
          draftId: content._id,
          data: tokenId
        }));
        console.log(`Sent minted for uniqueId=${connection.uniqueId}`)
      } else {
        console.log(`No connection for user ${content.ownerId}`)
      }
      const user = await User.findOne({_id: content.ownerId});
      const mint = new Mint({
        email: user.email,
        walletAddress: user.walletAddress,
        tokenId: tokenId
      });
      await mint.save();
    }
  }