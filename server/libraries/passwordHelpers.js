let bcrypt = require('bcrypt');

let hashPassword = async function (password, rounds = 10) {
  try {
    return await bcrypt.hash(password, rounds);
  } catch (e) {
    console.log(e);
    throw new Error("Failed at password hashing");
  }
};

let comparePasswords = async function (candidate, original) {
  try {
    return await bcrypt.compare(candidate, original);
  } catch (e) {
    console.log(e);
    throw new Error("User with the specified credentials does not exist");
  }
};

module.exports = {
  hashPassword,
  comparePasswords
};