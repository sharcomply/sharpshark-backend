const ethUtil = require('ethereumjs-util');
const crypto = require('crypto');

class ETH {
  constructor() {
  }

  generateNewPrivate() {
    const id = crypto.randomBytes(32).toString('hex');
    const add = ethUtil.privateToAddress(Buffer.from(id, "hex"));
    console.log(`New private key (to backend env): ${'0x' + id}`);
    console.log(`New public key (to contract): ${`0x` + add.toString('hex')}`);
  }

  isSignatureValid(code, signature, address) {
    const msgHex = ethUtil.bufferToHex(Buffer.from(code));
    const msgBuffer = ethUtil.toBuffer(msgHex);
    const msgHash = ethUtil.hashPersonalMessage(msgBuffer);
    const signatureBuffer = ethUtil.toBuffer(signature);
    const signatureParams = ethUtil.fromRpcSig(signatureBuffer);
    const publicKey = ethUtil.ecrecover(
      msgHash,
      signatureParams.v,
      signatureParams.r,
      signatureParams.s
    );
    const addressBuffer = ethUtil.publicToAddress(publicKey);
    const addressDecoded = ethUtil.bufferToHex(addressBuffer);

    return addressDecoded.toLowerCase() === address.toLowerCase()


    /* const hashedMessage = ethUtil.keccak(Buffer.from(code));
     const sigDecoded = ethUtil.fromRpcSig(signature)
     const decodedPublicKey = ethUtil.ecrecover(
       hashedMessage,
       sigDecoded.v,
       ethUtil.toBuffer(sigDecoded.r),
       ethUtil.toBuffer(sigDecoded.s)
     );
     console.log('0x' + ethUtil.pubToAddress(decodedPublicKey).toString('hex'));
     console.log(address);
     return '0x' + ethUtil.pubToAddress(decodedPublicKey).toString('hex') === address;*/
  }
}

module.exports = new ETH();
