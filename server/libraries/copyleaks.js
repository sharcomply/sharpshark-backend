const axios = require('axios').default;
const connections = require('../websocket/event-subscribers/connected-sockets');

async function getAccessToken() {
  try {
    let response = await axios.post('https://id.copyleaks.com/v3/account/login/api', {
      email: process.env.COPYLEAKS_EMAIL,
      key: process.env.COPYLEAKS_API_KEY
    })
    return response.data.access_token;
  } catch (e) {
    console.log(e.response.data);
    console.log(e.response.status);
    return false;
  }
}

async function submitScan(content, scan, network) {
  const connection = connections[scan.ownerId];
  let accessToken = await getAccessToken();
  if (!accessToken) {
    return false;
  }
  if (!scan.nonce) {
    console.log("Scan nonce was not provided");
    return false;
  }
  try {
    await axios.put(`https://api.copyleaks.com/v3/businesses/submit/url/${scan._id}`, {
      url: content.sanitizedLinkUrl,
      properties: {
        developerPayload: `${scan.nonce}_${scan.ownerId}_${scan.purpose}_${network}`,
        sandbox: process.env.IS_SCAN_SANBOXED === 'true',
        expiration: 256,
        webhooks: {
          status: `${process.env.HTTP_PROTOCOL}://${process.env.SERVER_IP}/api/v1/copyleaks/webhook-ws/:status`
        }
      }
    }, {
      headers: {
        'Authorization': `Bearer ${accessToken}`
      }
    })
    if (connection) {
      connection.send(JSON.stringify({
        action: 'scanCurrentStatus', // 2.a
        status: 'sendToCheckSuccess',
        scanId: scan._id,
        draftId: content._id,
      }));
      console.log(`Sent sendToCheckSuccess for uniqueId=${connection.uniqueId}`)
    } else {
      console.log(`No connection for user ${scan.ownerId}`)
    }
    return true;
  } catch (e) {
    // if (connection) {
    //   connection.send(JSON.stringify({
    //     action: 'scanCurrentStatus', // 2.b
    //     status: 'sendToCheckError',
    //     scanId: scan._id,
    //     draftId: content._id,
    //     data: e.response.data
    //   }));
    // } else {
    //   console.log(`No connection for user ${scan.ownerId}`)
    // }
    console.log(`Copyleaks error:`);
    console.log(e.response.data);
    console.log(e.response.status);
    console.log(`Resending webhook...`);
    try {
      await axios.post(`https://api.copyleaks.com/v3/businesses/scans/${scan._id}/webhooks/resend`, {}, {
        headers: {
          'Authorization': `Bearer ${accessToken}`
        }
      });
    } catch (e) {
      // pass
    }
    return false;
  }
}

async function estimateScan(content, scan, network) {
  const connection = connections[scan.ownerId];
  try {
    let accessToken = await getAccessToken();
    if (!accessToken) {
      return false;
    }
    if (!scan.nonceEstimate) {
      console.log("Scan nonce was not provided");
      return false;
    }
    await axios.put(`https://api.copyleaks.com/v3/businesses/submit/url/${scan.nonceEstimate}`, {
      url: content.linkUrl,
      properties: {
        developerPayload: `${scan.nonceEstimate}_${scan.ownerId}_${scan.purpose}_${network}`,
        sandbox: process.env.IS_SCAN_SANBOXED === 'true',
        expiration: 256,
        action: 1,
        webhooks: {
          status: `${process.env.HTTP_PROTOCOL}://${process.env.SERVER_IP}/api/v1/copyleaks/webhook-ws/:status`
        }
      }
    }, {
      headers: {
        'Authorization': `Bearer ${accessToken}`
      }
    })
    if (connection) {
      connection.send(JSON.stringify({
        action: 'scanEstimateCurrentStatus',
        status: 'sendToEstimateSuccess',
        scanId: scan._id,
        draftId: content._id,
      }));
      console.log(`Sent sendToEstimateSuccess for uniqueId=${connection.uniqueId}`)
    } else {
      console.log(`No connection for user ${scan.ownerId}`)
    }
    return true;
  } catch (e) {
    if (connection) {
      connection.send(JSON.stringify({
        action: 'scanEstimateCurrentStatus',
        status: 'sendToEstimateError',
        scanId: scan._id,
        draftId: content._id,
        data: e.response.data
      }));
      console.log(`Sent sendToEstimateError for uniqueId=${connection.uniqueId}`)
    } else {
      console.log(`No connection for user ${scan.ownerId}`)
    }
    console.log(e.response.data);
    console.log(e.response.status);
    return false;
  }
}

exports.submitScan = submitScan;
exports.estimateScan = estimateScan;