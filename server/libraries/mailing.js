const nodemailer = require("nodemailer");

let transporter = nodemailer.createTransport({
  host: process.env.SMTP_HOST,
  port: process.env.SMTP_PORT,
  secure: false, // true for 465, false for other ports
  auth: {
    user: process.env.SMTP_USER,
    pass: process.env.SMTP_PASSWORD
  }
});

postfixSend = async function postfixSend(emailInfo) {
  try {
    await transporter.sendMail({
      from: process.env.SENDER_NAME + ' <' + process.env.SUPPORT_EMAIL + '>', // sender address
      to: emailInfo.to, // list of receivers
      subject: emailInfo.subject, // Subject line
      text: emailInfo.text, // plain text body,
      replyTo: emailInfo.replyTo || process.env.SUPPORT_EMAIL,
      html: emailInfo.html // html body
    });
  } catch (err) {
    console.log(err);
  }
};

module.exports = postfixSend;