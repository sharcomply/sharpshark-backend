const mongoose = require('mongoose');

const localeSchema = mongoose.Schema({
  name: {
    type: String
  },
  link: {
    type: String,
  },
});

const Locale = mongoose.model('Locale', localeSchema);

module.exports = Locale;
