const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const ApplicationError = require('../../utils/application-error');
const crypto = require('crypto');

const _emailRegExp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const hashPassword = function (password) {
  if (!this.salt) {
    this.salt = crypto.randomBytes(16).toString('hex');
  }
  const hash = crypto.createHmac('sha512', this.salt);
  hash.update(password);
  return hash.digest('hex').toString();
};

const adminSchema = mongoose.Schema({
  email: {
    type: String,
    lowercase: true,
    unique: true,
    sparse: true,
    index: true,
    validate: function (value) {
      return _emailRegExp.test(value);
    }
  },
  salt: {
    type: String,
    select: false
  },
  password: {
    type: String,
    select: false,
    set: hashPassword
  },
});

adminSchema.methods.getJWT = function () {
  let authToken = jwt.sign({
    id: this._id,
    role: 'admin',
  }, process.env.JWT_SECRET, {expiresIn: "1d"});
  return {authToken};
};

adminSchema.methods.authenticate = async function(password) {
  if (!password) {
    throw ApplicationError.InvalidCredentials();
  }
  const hash = crypto.createHmac('sha512', this.salt);
  hash.update(password);
  const pw = hash.digest('hex').toString();
  if (pw === this.password) {
    return this.getJWT();
  } else {
    throw ApplicationError.InvalidCredentials();
  }
};


const Admin = mongoose.model('Admin', adminSchema);

module.exports = Admin;
