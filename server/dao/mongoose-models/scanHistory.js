const mongoose = require('mongoose');
const crypto = require('crypto');

const ObjectId = mongoose.Schema.Types.ObjectId;

function nonceGen() {
  return crypto.randomBytes(32).toString('hex')
}
function estimateNonceGen() {
  return crypto.randomBytes(18).toString('hex')
}

const scanHistorySchema = mongoose.Schema({
  ownerId: {
    type: ObjectId,
    ref: 'User'
  },
  documentId: {
    type: ObjectId,
    ref: 'Content'
  },
  requestId: {
    type: String,
  },
  purpose: {
    type: String,
  },
  aggregatedScore: {
    type: Number,
    default: -1
  },
  similarContent: [{
    url: String,
    title: String,
    introduction: String,
    similarity: Number
  }],
  isRequestPayed: {
    type: Boolean,
    default: false
  },
  nonce: {
    type: String,
    default: nonceGen,
    select: false
  },
  nonceEstimate: {
    type: String,
    default: estimateNonceGen,
    select: false
  },
  pagesCount: {
    type: Number,
    default: -1
  },
  isCopyleaksWebhooked: {
    type: Boolean,
    default: false
  },
  isResultIgnored: {
    type: Boolean,
    default: false
  }
});

const ScanHistory = mongoose.model('ScanHistory', scanHistorySchema);

module.exports = ScanHistory;
