const mongoose = require('mongoose');

const ObjectId = mongoose.Schema.Types.ObjectId;

const monitoringSchema = mongoose.Schema({
  documentId: {
    type: ObjectId,
    ref: 'Content'
  },
  isPayed: {
    type: Boolean
  },
  isActive: {
    type: Boolean,
  },
  scansTotal: {
    type: Number
  },
  scansLeft: {
    type: Number
  },
  requestId: {
    type: String,
  },
});

const Monitoring = mongoose.model('Monitoring', monitoringSchema);

module.exports = Monitoring;
