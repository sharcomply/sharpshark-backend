const mongoose = require('mongoose');
const { v4: uuidv4 } = require('uuid');

const ObjectId = mongoose.Schema.Types.ObjectId;

const sessionShuftiproSchema = mongoose.Schema({
  userId: {
    type: ObjectId,
    ref: 'User'
  },
  generatedUserId: {
    type: String,
    default: uuidv4
  },
  responseRequest: {
    type: JSON,
    default: {}
  }
}, {
  timestamps: true
});

const SessionShuftipro = mongoose.model('SessionShuftipro', sessionShuftiproSchema);

module.exports = SessionShuftipro;
