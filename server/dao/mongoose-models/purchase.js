const mongoose = require('mongoose');

const purchaseSchema = mongoose.Schema({
    email: {
      type: String,
      default: '',
    },
    walletAddress: {
      type: String,
      default: '',
    },
    status: {
      type: String,
      enum: ['not processed', 'processing', 'processed'],
      default: 'not processed'
    },
    amount: {
      type: Number,
      default: 0
    }
  },
  {
    timestamps: true
  });

const Purchase = mongoose.model('Purchase', purchaseSchema);

module.exports = Purchase;
