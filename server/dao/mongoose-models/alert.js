const mongoose = require('mongoose');

const ObjectId = mongoose.Schema.Types.ObjectId;

const alertSchema = mongoose.Schema({
  type: {
    type: String,
    enum: ["monitoring"]
  },
  scanId: {
    type: ObjectId,
    ref: 'ScanHistory'
  },
  ownerId: {
    type: ObjectId,
    ref: 'User'
  },
  documentId: {
    type: ObjectId,
    ref: 'Content'
  },
  date: {
    type: Date
  },
  isRead: {
    type: Boolean,
    default: false
  },
  isSkipped: {
    type: Boolean,
    default: false
  },
  isReacted: {
    type: Boolean,
    default: false
  },
  report: {
    type: String,
    default: ''
  }
});

const Alert = mongoose.model('Alert', alertSchema);

module.exports = Alert;
