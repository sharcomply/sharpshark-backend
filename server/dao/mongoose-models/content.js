const mongoose = require('mongoose');

const ObjectId = mongoose.Schema.Types.ObjectId;

const contentSchema = mongoose.Schema({
  type: {
    type: String,
    enum: ["image", "binaryFile", "text"]
  },
  isDeleted: {
    type: Boolean
  },
  deleteDate: {
    type: Date
  },
  linkUrl: {
    type: String
  },
  sourceHash: {
    type: String
  },
  metadataUrl: {
    type: String
  },
  metadataHash: {
    type: String
  },
  tokenId: {
    type: String
  },
  isMinted: {
    type: Boolean,
    default: false
  },
  title: {
    type: String,
    default: ''
  },
  fullTitle: {
    type: String,
    default: ''
  },
  ownerId: {
    type: ObjectId,
    ref: 'User'
  },
  oldVersion: {
    type: ObjectId,
    ref: 'Content'
  },
  authors: [{
    type: String
  }],
  coAuthors: [{
    type: String
  }],
  participants: [{
    type: String
  }],
  commonId: {
    type: String,
  },
  version: Number,
  parentTokenId: String,
  childId: {
    type: ObjectId,
    ref: 'Content'
  },
  parentId: {
    type: ObjectId,
    ref: 'Content'
  },

  linkUrlQrCodeLink: String,
  metadataUrlQrCodeLink: String,

  mintedNetworks: [
    mongoose.Schema({
      name: String,
      mintTransactionHash: String,
      mintTransactionQrCodeLink: String,
      mintTransactionLink: String,
      blockchainTokenUrlQrCodeLink: String,
      blockchainTokenUrlLink: String,
    })
  ],
  additionalImages: [{
    type: String
  }],
  images: [{
    type: String
  }],
  sanitizedLinkUrl: {
    type: String,
    default: ''
  },
  pagesCount: {
    type: Number,
    default: -1
  },
  cachedMetadata: {
    type: mongoose.Schema({
      Title: String,
      Authors: [{type: String}],
      CoAuthors: [{type: String}],
      Participants: [{type: String}],
      MetamaskMinter: String,
      SourceIPFSHash: String,
      SourceIPFSUri: String
    }),
    default: {}
  },
  cachedSource: {
    type: String,
    default: ''
  },
}, {
  timestamps: true
});

const Content = mongoose.model('Content', contentSchema);

module.exports = Content;
