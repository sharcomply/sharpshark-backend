const mongoose = require('mongoose');

const mintSchema = mongoose.Schema({
    email: {
      type: String,
      default: '',
    },
    walletAddress: {
      type: String,
      default: '',
    },
    tokenId: {
      type: String,
      default: '',
    }
  },
  {
    timestamps: true
  });

const Mint = mongoose.model('Mint', mintSchema);

module.exports = Mint;
