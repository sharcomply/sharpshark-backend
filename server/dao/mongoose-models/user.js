const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const _emailRegExp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const userSchema = mongoose.Schema({
    email: {
      type: String,
      lowercase: true,
      unique: true,
      sparse: true,
      index: true,
      validate: function(value) {
        return _emailRegExp.test(value);
      }
    },
    isEmailVerified: Boolean,
    firstName: {
      type: String,
      default: ''
    },
    lastName: {
      type: String,
      default: ''
    },
    language: {
      type: String,
      default: ''
    },
    country: {
      type: String,
      default: ''
    },
    nonce: {
      type: String
    },
    walletAddress: {
      type: String,
      required: true
    },
    walletAddressNear: {
      type: String,
      default: ''
    },
    isVerification: {
      type: Boolean,
      default: false
    },
    disputesPrefillEmail:{
      type: String,
      default: ''
    }
  },
  {
    timestamps: true
  });

userSchema.index({ walletAddress: 1, networkType: 1}, { unique: true });

userSchema.methods.getJWT = function () {
  let authToken = jwt.sign({
    id: this._id,
    role: 'user',
    walletAddress: this.walletAddress,
    networkType: this.networkType,
    isVerification: this.isVerification,
  }, process.env.JWT_SECRET, { expiresIn: "1d" });
  return { authToken };
};

const User = mongoose.model('User', userSchema);

module.exports = User;
