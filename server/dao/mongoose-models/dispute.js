const mongoose = require('mongoose');

const ObjectId = mongoose.Schema.Types.ObjectId;

const disputeSchema = mongoose.Schema({
  alertId: {
    type: ObjectId,
    ref: 'Alert'
  },
  documentId: {
    type: ObjectId,
    ref: 'Content'
  },
  discoveredDate: {
    type: Date
  },
  appearedOn: {
    type: String
  },
  demands: {
    type: String,
    enum: ["Remove", "Pay and/or credit", "Pay and remove", "Pay and attribute"]
  },
  demandsProperly: {
    type: Boolean,
    default: false
  },
  demandsChange: {
    type: Boolean,
    default: false
  },
  demandsDont: {
    type: Boolean,
    default: false
  },
  demandsMakeSure: {
    type: Boolean,
    default: false
  },
  payAmount: {
    type: Number,
    default: -1
  },

  publicationQuestionScreenshots: [{
    type: String
  }],
  publicationQuestionLinks: [{
    type: String
  }],
  publicationQuestionWebarchiveLinks: [{
    type: String
  }],
  isPublicationQuestionWebarchiveFailed: {
    type: Boolean,
    default: false
  },

  publicationLinks: [{
    type: String
  }],
  publicationWebarchiveLinks: [{
    type: String
  }],
  isPublicationWebarchiveFailed: {
    type: Boolean,
    default: false
  },

  draftsScreenshots: [{
    type: String
  }],

  ownerClaimText: {
    type: String,
    default: ''
  },
  providerClaimText: {
    type: String,
    default: ''
  },
  googleClaimText: {
    type: String,
    default: ''
  },

  ownerClaimEmails: [{
    type: String
  }],
  ownerClaimType: {
    type: String,
    enum: ["By Email", "Via Form", "Nothing Found"]
  },

  providerClaimEmails: [{
    type: String
  }],

  status: {
    type: String,
    enum: [
      'Draft: Website Owner',
      'In Progress: Website Owner',
      'Letter Sent: Website Owner',
      'Claim Sent: Website Owner',
      'Draft: Provider',
      'In Progress: Provider',
      'Letter Sent: Provider',
      'Claim Sent: Provider',
      'Draft: Google',
      'In Progress: Google',
      'Letter Sent: Google',
      'Claim Sent: Google',
      'Under Court',
      'Closed',
    ]
  },
  ownerId: {
    type: ObjectId,
    ref: 'User'
  },

  isNotRelevant: {
    type: Boolean,
    default: false
  },
  userContactEmail: {
    type: String,
    default: ''
  },
  isDeleted: {
    type: Boolean
  },
  deleteDate: {
    type: Date
  },
},{
  timestamps: true
});

const Dispute = mongoose.model('Dispute', disputeSchema);
module.exports = Dispute;
