const getCryptoExchangeCourse = require('./get-crypto-exchange-course');
const BN = require('bignumber.js');

module.exports = async (amount, network) => {
  amount = new BN(amount);
  const course = await getCryptoExchangeCourse(network);
  const pricePerToken = new BN(Number.parseInt(Number.parseFloat(course.price) * 10000));
  const ten = new BN(10);
  let tenInEighteenPow;
  if (network === 'near') {
    tenInEighteenPow = ten.pow(24);
  } else {
    tenInEighteenPow = ten.pow(18);
  }
  return amount.multipliedBy(pricePerToken).dividedBy(tenInEighteenPow).dividedBy(10000).dp(3).toString();
}