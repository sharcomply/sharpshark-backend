const Promise = require("bluebird");
const generateAndUploadQr = require("./generate-and-upload-qr");


module.exports = async (content, network, transactionHash) => {
  const oldMintedNetworkIndex = content.mintedNetworks.findIndex(item => item.name === network);
  if (oldMintedNetworkIndex > -1) {
    content.mintedNetworks.splice(oldMintedNetworkIndex, 1);
  }
  const mintedNetwork = {name: network};
  mintedNetwork.mintTransactionHash = transactionHash;

  let explorer, contractAddress;
  switch (network) {
    case 'bsc':
      explorer = process.env.BSC_BLOCKCHAIN_EXPLORER;
      contractAddress = process.env.BSC_CONTRACT_ADDRESS;
      break;
    case 'eth':
      explorer = process.env.ETHEREUM_BLOCKCHAIN_EXPLORER;
      contractAddress = process.env.ETHEREUM_CONTRACT_ADDRESS;
      break;
    case 'polygon':
      explorer = process.env.POLYGON_BLOCKCHAIN_EXPLORER;
      contractAddress = process.env.POLYGON_CONTRACT_ADDRESS;
      break;
    case 'near':
      explorer = process.env.NEAR_BLOCKCHAIN_EXPLORER;
      contractAddress = process.env.NEAR_CONTRACT_ADDRESS;
      break;
  }

  if (network === 'near') {
    mintedNetwork.mintTransactionLink = `${explorer}/?query=${transactionHash}`;
    mintedNetwork.blockchainTokenUrlLink = mintedNetwork.mintTransactionLink;
  } else {
    mintedNetwork.mintTransactionLink = `${explorer}/tx/${transactionHash}`;
    mintedNetwork.blockchainTokenUrlLink = `${explorer}/token/${contractAddress}?a=${content.tokenId}`;
  }

  const [
    mintTransactionQrCodeLink,
    linkUrlQrCodeLink,
    metadataUrlQrCodeLink,
    blockchainTokenUrlQrCodeLink,
  ] = await Promise.all([
    generateAndUploadQr('tx', mintedNetwork.mintTransactionLink, content.ownerId, content._id, network),
    generateAndUploadQr('source', content.linkUrl, content.ownerId, content._id),
    generateAndUploadQr('meta', content.metadataUrl, content.ownerId, content._id),
    generateAndUploadQr('token', mintedNetwork.blockchainTokenUrlLink, content.ownerId, content._id, network),
  ]);

  mintedNetwork.mintTransactionQrCodeLink = mintTransactionQrCodeLink;
  content.linkUrlQrCodeLink = linkUrlQrCodeLink;
  content.metadataUrlQrCodeLink = metadataUrlQrCodeLink;
  mintedNetwork.blockchainTokenUrlQrCodeLink = blockchainTokenUrlQrCodeLink;
  content.mintedNetworks.push(mintedNetwork);
}