module.exports = [
  {
    $group: {
      _id: {
        documentId: "$documentId",
        appearedOn: "$appearedOn"
      },
      disputes: {$push: "$$ROOT"}
    }
  },
  {
    $group: {
      _id: "$_id.documentId",
      appearedOns: {
        $push: {
          appearedOn: "$_id.appearedOn",
          disputes: "$disputes"
        }
      }
    }
  },
  {
    $project: {
      documentId: "$_id",
      appearedOns: 1,
      _id: 0
    }
  },
  {
    $unwind: "$appearedOns"
  },
  {
    $unwind: "$appearedOns.disputes"
  },
  {
    $lookup: {
      from: "alerts",
      localField: "appearedOns.disputes.alertId",
      foreignField: "_id",
      as: "appearedOns.disputes.alertId"
    }
  },
  {
    $unwind: "$appearedOns.disputes.alertId"
  },
  {
    $lookup: {
      from: "scanhistories",
      localField: "appearedOns.disputes.alertId.scanId",
      foreignField: "_id",
      as: "appearedOns.disputes.alertId.scanId"
    }
  },
  {
    $unwind: "$appearedOns.disputes.alertId.scanId"
  },
  {
    $group: {
      _id: {
        documentId: "$documentId",
        appearedOn: "$appearedOns.appearedOn"
      },
      disputes: {$push: "$appearedOns.disputes"}
    }
  },
  {
    $group: {
      _id: "$_id.documentId",
      appearedOns: {
        $push: {
          appearedOn: "$_id.appearedOn",
          disputes: "$disputes"
        }
      }
    }
  },
  {
    $project: {
      documentId: "$_id",
      appearedOns: 1,
      _id: 0
    }
  },
  {
    $lookup: {
      from: "contents",
      localField: "documentId",
      foreignField: "_id",
      as: "documentId"
    }
  },
  {
    $unwind: "$documentId"
  }
];