const ethers = require("ethers");
const getCryptoExchangeCourse = require('./get-crypto-exchange-course');

module.exports = async (amount, network, multiplier = 1) => {
  amount = ethers.BigNumber.from(Number.parseInt(Math.ceil(Number.parseFloat(amount) * 10000)));
  const course = await getCryptoExchangeCourse(network);
  const pricePerToken = ethers.BigNumber.from(Number.parseInt(Number.parseFloat(course.price) * 10000));
  const ten = ethers.BigNumber.from(10);
  let tenInEighteenPow;
  if (network === 'near') {
    tenInEighteenPow = ten.pow(24);
  } else {
    tenInEighteenPow = ten.pow(18);
  }
  const multiplierBn = ethers.BigNumber.from(multiplier);
  return amount.mul(tenInEighteenPow).mul(multiplierBn).div(pricePerToken).toString();
}