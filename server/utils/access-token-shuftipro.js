const axios = require('axios');
const SessionShuftipro = require('mongoose').models.SessionShuftipro;
const ApplicationError = require('../utils/application-error');

module.exports = async (req) => {
  const token = Buffer.from(`${process.env.SHUFTIPRO_CLIENT_ID}:${process.env.SHUFTIPRO_SECRET_KEY}`).toString('base64'); //BASIC AUTH TOKEN
  const options = {
    method: 'post',
    url: 'https://api.shuftipro.com/get/access/token',
    headers:
      {
        'Accept'        : 'application/json',
        'Content-Type'  : 'application/json',
        'Authorization' : 'Basic '+token
      },
    json: true };
  const result = await axios(options);
  if (result) {
    const newSession = new SessionShuftipro({
      userId: req.jwt.id
    });
    await newSession.save();
    return {accessToken: result.data.access_token, data: newSession};
  } else {
    throw ApplicationError.ServiceError();
  }
}
