const BN = require("bn.js");
module.exports = (expire) => (new BN(expire)).mul(new BN("1000000000")).toString(10);