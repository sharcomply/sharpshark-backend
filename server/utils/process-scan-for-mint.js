const connections = require("../websocket/event-subscribers/connected-sockets");
const ApplicationErrors = require("./application-error");
const axios = require("axios");
const upload = require("./upload-to-nft-storage");
const BN = require("bn.js");
const ethSign = require("../blockchain/ethereum/signature");
const nearSign = require("../blockchain/near/signature");
const expireToNanoSec = require("./expire-to-nano-sec");
const generateLogoQr = require("./generate-logo-qr");
const generateAndUploadQr = require("./generate-and-upload-qr");
const User = require('mongoose').models.User;

module.exports = async (scan, content, network) => {
  const owner = await User.findOne({_id: content.ownerId});
  const connection = connections[owner._id];
  try {

    if (process.env.NODE_ENV === "production") {
    // && scan.aggregatedScore >= 30
      scan.similarContent.forEach(sc => {
        if (sc.similarity >= 30 && !scan.isResultIgnored) {
          throw ApplicationErrors.TooLowAggregatedScore(sc.similarity);
        }
      });
    }

    if (connection) {
      connection.send(JSON.stringify({
        action: 'scanCurrentStatus',
        status: 'preparingWork',
        scanId: scan._id,
        draftId: content._id,
      }));
      console.log(`Sent preparingWork for uniqueId=${connection.uniqueId}`)
    } else {
      console.log(`No connection for user ${owner._id}`)
    }

    // 2. Заливаем файл с s3 на IPFS (ссылка на файл лежит в linkUrl) (если ссылка не уже на IPFS)
    let metadata;
    if (!content.linkUrl.startsWith(`https://ipfs.io/ipfs/`)) {
      const response = await axios.get(content.linkUrl, {responseType: 'arraybuffer'});
      const sourceBuffer = Buffer.from(response.data, 'utf-8');
      content.cachedSource = response.data.length > 1000000 ? content.linkUrl : response.data;
      let sourceCid;
      let attempt = 0;
      while (!sourceCid || attempt <= 3) {
        try {
          sourceCid = await upload(sourceBuffer, content.type === 'image');
          attempt += 1;
        } catch (e) {
          // pass
          attempt += 1;
        }
        await new Promise(r => setTimeout(r, 2000));
      }
      if (!sourceCid) {
        throw ApplicationErrors.NftStorageUploadError();
      }
      content.linkUrl = 'https://ipfs.io/ipfs/' + sourceCid.toString();
      content.sourceHash = sourceCid.toString();

      // 3. Генерим токенId(хз подут ли случайные байты? Если нам сильно хочется можем юзать байты из objectId + недостающие до int256) (если токенid уже не прописан в БД)
      content.tokenId = new BN(content._id.toString() + scan._id.toString(), 16);

      // 1. Готовим JSON файл и льем его на IPFS. (если его еще нет в БД записи)
      const websiteCertLink = `${process.env.HTTP_PROTOCOL}://${process.env.SERVER_IP}/certificate/${content.tokenId}`;
      const qrCodeCertLink = await generateAndUploadQr('website', websiteCertLink, content.ownerId, content._id, 'all', true);
      metadata = {
        Title: content.fullTitle,
        Authors: content.authors ? content.authors : [],
        CoAuthors: content.coAuthors ? content.coAuthors : [],
        Participants: content.participants ? content.participants : [],
        MetamaskMinter: owner.walletAddress,
        SourceIPFSHash: content.sourceHash,
        SourceIPFSUri: content.linkUrl,
        image: qrCodeCertLink,
      }
      const metadataBuffer = Buffer.from(JSON.stringify(metadata));
      const metadataCid = await upload(metadataBuffer);
      content.metadataHash = metadataCid.toString();
      content.metadataUrl = 'https://ipfs.io/ipfs/' + metadataCid.toString();

      content.cachedMetadata = metadata;
    }

    if (connection) {
      connection.send(JSON.stringify({
        action: 'scanCurrentStatus', // 3.a
        status: 'metadataUploaded',
        scanId: scan._id,
        draftId: content._id,
        data: content.metadataUrl
      }));
      console.log(`Sent metadataUploaded for uniqueId=${connection.uniqueId}`)
    } else {
      console.log(`No connection for user ${owner._id}`)
    }

    // 4. Пишем в БД ссылки новые (linkUrl перезаписываем на путь к файлу на IPFS) и токенИд (если опять же это не повторный скан и оно уже заливалось)
    await content.save();

    // 5. Делаем подпись с ipfs хешами, адресом кошелька, токенId, датой минта максимальной.
    const expireDate = new Date();
    expireDate.setDate(expireDate.getDate() + 1); // 24 hours expire
    const expire = Math.floor(expireDate.getTime() / 1000);
    const parentTokenId = content.parentTokenId ? content.parentTokenId : "0";
    const version = content.version;

    let userResponse;
    if (network === 'near') {
      const [signature, v] = await nearSign.signForMint(
        content.tokenId.toString(10),
        owner.walletAddressNear,
        content.sourceHash,
        content.metadataHash,
        expire,
        parentTokenId,
        version
      );
      // 6. Если юзер подключен у нас к ws ( if (connections[ownerId])) - то шлем ему подпись. Если нет - подпись сгорает просто и ничего не делаем. Подпись не запоминаем - будет еще раз протект юзать.
      userResponse = {
        token_id: content.tokenId.toString(10),
        receiver_id: owner.walletAddressNear,
        content_hash: content.sourceHash,
        metadata_hash: content.metadataHash,
        expire: expireToNanoSec(expire),
        parent_token_id: parentTokenId,
        version,
        signature,
        v,
        metadata
      }
    } else {
      const signature = await ethSign.signForMint(
        content.tokenId.toString(10),
        owner.walletAddress,
        content.sourceHash,
        content.metadataHash,
        expire,
        parentTokenId,
        version
      );
      // 6. Если юзер подключен у нас к ws ( if (connections[ownerId])) - то шлем ему подпись. Если нет - подпись сгорает просто и ничего не делаем. Подпись не запоминаем - будет еще раз протект юзать.
      userResponse = {
        tokenId: content.tokenId.toString(10),
        to: owner.walletAddress,
        contentHash: content.sourceHash,
        metadataHash: content.metadataHash,
        parentTokenId,
        version,
        expire: expire,
        signature: signature
      }
    }

    if (connection) {
      connection.send(JSON.stringify({
        action: 'scanCurrentStatus', // 4
        status: 'scanSuccess',
        scanId: scan._id,
        draftId: content._id,
        data: userResponse
      }));
      console.log(`Signature sent. Here we go: ${JSON.stringify(userResponse)}`);
      console.log(`Sent scanSuccess for uniqueId=${connection.uniqueId}`)
    } else {
      console.log(`There was no connection for user ${owner._id}, but here is signature: ${JSON.stringify(userResponse)}`);
    }
  } catch (e) {
    console.log(`Something went wrong during signature generating. Take your error:`, e);
    if (connection) {
      connection.send(JSON.stringify({
        action: 'scanCurrentStatus', // 3.b
        status: 'scanFailed',
        scanId: scan._id,
        draftId: content._id,
        data: e
      }));
      console.log(`Sent scanFailed for uniqueId=${connection.uniqueId}`)
    } else {
      console.log(`No connection for user ${owner._id}`)
    }
  }
}
