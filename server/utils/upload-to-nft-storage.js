const {NFTStorage, Blob} = require('nft.storage');

module.exports = async (buffer, isImage) => {
  const storage = new NFTStorage({ token: process.env.NFT_STORAGE_API_KEY });
  const BOM = new Uint8Array([0xEF,0xBB,0xBF]); // https://stackoverflow.com/questions/6672834/specifying-blob-encoding-in-google-chrome
  const data = isImage ? [buffer] : [BOM, buffer];
  const { cid, car } = await NFTStorage.encodeBlob(new Blob(data));
  await storage.storeCar(car);
  return cid;
}