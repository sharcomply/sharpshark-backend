const camelCase = require("camelcase");
const schemas = require("../api/schemas");
const ApplicationError = require("./application-error");

function validate(req){
  const test = req.route.path;
  const url = req.route.path.split(/\/(?=:)/)[0].split('/');
  let nameFile = camelCase(url[url.length-1]);
  if (schemas.hasOwnProperty(nameFile)){
    let validationSchema = schemas[nameFile];
    const { error } = validationSchema.validate(req.body, {stripUnknown: true});
    const valid = error == null;
    if (!valid) {
      const errObject = {
        fieldName: "What validation was thrown",
        message: "Validation error, check errors object"
      };
      throw ApplicationError.JsonValidation(errObject);
    }
  }
}

module.exports = validate;
