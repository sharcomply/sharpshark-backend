const axios = require("axios");
const apis = {
  'bsc': 'https://api.binance.com/api/v3/ticker/price?symbol=BNBUSDT',
  'eth': 'https://api.binance.com/api/v3/ticker/price?symbol=ETHUSDT',
  'polygon': 'https://api.binance.com/api/v3/ticker/price?symbol=MATICUSDT',
  'near': 'https://api.binance.com/api/v3/ticker/price?symbol=NEARUSDT',
}

module.exports = async (network) => {
  const response = await axios.get(apis[network]);
  return response.data;
}