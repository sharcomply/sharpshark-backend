const sanitizeHtml = require('sanitize-html');
const jsdom = require("jsdom");
const { JSDOM } = jsdom;

module.exports = async (html) => {
  const result = {
    html: html,
    sanitized: sanitizeHtml(html, {
      allowedTags: [],
      allowedAttributes: {}
    }),
    images: []
  };
  const dom = new JSDOM(html);
  const tags = [...dom.window.document.querySelectorAll('img')];
  result.images = tags.map(item => item.src);
  result.images = result.images.filter(item => item.startsWith('http'));
  return result;
}