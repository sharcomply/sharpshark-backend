const AWS = require("aws-sdk");
const mime = require('mime-types');

const client = new AWS.S3({
  signatureVersion: 'v4',
  region: process.env.S3_REGION || 'eu-west-1'
});
const bucket = process.env.S3_BUCKET_NAME;

module.exports = async (buffer, filename) => {
  const params = {
    Bucket: bucket,
    Key: filename,
    Body: buffer,
    ContentType: mime.lookup(filename),
    CacheControl: 'no-cache'
  };

  await client.upload(params).promise();
  return `https://${process.env.CDN_NAME}/${filename}`;
};
