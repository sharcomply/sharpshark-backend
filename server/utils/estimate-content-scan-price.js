const ApplicationErrors = require("./application-error");
const pricePerPage = Number.parseFloat(process.env.SCAN_PRICE_TEXT);
const pricePerImage = Number.parseFloat(process.env.SCAN_PRICE_IMAGE);

module.exports = async (content) => {
  if (content.type === 'text') {
    if (content.pagesCount === -1) {
      throw ApplicationErrors.NotFound();
    }
    if (content.pagesCount === -1) {
      throw ApplicationErrors.NotFound();
    }
    let price = content.pagesCount * pricePerPage;
    if (content.additionalImages && content.additionalImages.length) {
      price += content.additionalImages.length * pricePerImage;
    }
    return price * 2;
  }
  if (content.type === 'image') {
    return pricePerImage * 2;
  }
  throw ApplicationErrors.PriceEstimatingError();
}