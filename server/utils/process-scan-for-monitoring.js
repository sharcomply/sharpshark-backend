const connections = require("../websocket/event-subscribers/connected-sockets");
const Monitoring = require('mongoose').models.Monitoring;
const Alert = require('mongoose').models.Alert;
const Content = require('mongoose').models.Content;

module.exports = async (scan) => {
  const ownerId = scan.ownerId._id ? scan.ownerId._id : scan.ownerId;
  const documentId = scan.documentId._id ? scan.documentId._id : scan.documentId;

  const monitoring = await Monitoring.findOne({documentId: documentId, isActive: true});
  if (!monitoring) {
    console.log(`Monitoring for doc ${documentId} not found`);
    return;
  }
  monitoring.scansLeft -= 1;
  if (monitoring.scansLeft <= 0) {
    monitoring.isActive = false;
  }
  await monitoring.save();
  const alert = new Alert({
    date: new Date(),
    scanId: scan._id,
    type: 'monitoring',
    documentId: documentId,
    ownerId: ownerId
  });
  await alert.save();
  alert.scanId = scan._id;
  const connection = connections[ownerId];
  const draft = await Content.findOne({_id: documentId});
  if (connection) {
    connection.send(JSON.stringify({
      action: 'monitoringStatus',
      status: 'alertCreated',
      alert: alert,
      draftId: draft,
    }));
    console.log(`Sent alertCreated for uniqueId=${connection.uniqueId}`)
  } else {
    console.log(`No connection for user ${ownerId}`)
  }
}