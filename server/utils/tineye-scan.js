const connections = require("../websocket/event-subscribers/connected-sockets");
const tineye = require('../configs/tineye');
const processScanForMint = require("./process-scan-for-mint");
const processScanForMonitoring = require("./process-scan-for-monitoring");

module.exports = async (content, scan, purpose, network) => {
  const connection = connections[content.ownerId];
  try {
    const response = await tineye.searchUrl(content.linkUrl);
    if (connection) {
      connection.send(JSON.stringify({
        action: 'scanCurrentStatus', // 2.a
        status: 'sendToCheckSuccess',
        scanId: scan._id,
        draftId: content._id,
      }));
      console.log(`Sent sendToCheckSuccess for uniqueId=${connection.uniqueId}`)
    } else {
      console.log(`No connection for user ${content.ownerId}`)
    }
    scan.aggregatedScore = response.results.matches.length > 0 ? response.results.matches[0].score : 0;
    const result = Math.random();
    scan.similarContent = response.results.matches.map(item => {
      return {
        url: item.backlinks[0].url,
        title: item.domain,
        introduction: item.backlinks[0].backlink,
        similarity: item.score
      }
    });
    if (process.env.TINEYE_API_KEY === '6mm60lsCNIB,FwOWjJqA80QZHh9BMwc-ber4u=t^' && result < 0.9) {
      // if test api key, chance for bad score = 50% 
      scan.aggregatedScore = 0;
      scan.similarContent = [];
    }
    await scan.save();
  } catch (e) {
    console.log(e);
    connection.send(JSON.stringify({
      action: 'scanCurrentStatus', // 2.b
      status: 'sendToCheckError',
      scanId: scan._id,
      draftId: content._id,
      data: e.message
    }));
    console.log(`Sent sendToCheckError for uniqueId=${connection.uniqueId}`)
  }

  if (purpose === 'mint') {
    await processScanForMint(scan, content, network);
  } else if (purpose === 'monitoring') {
    await processScanForMonitoring(scan);
  }
}