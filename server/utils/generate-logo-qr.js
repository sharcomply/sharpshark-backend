const {QRCodeCanvas} = require('@loskir/styled-qr-code-node'); // or CommonJS

module.exports = async (content) => {
  const qrCode = new QRCodeCanvas({
    width: 1200,
    height: 1200,
    type: "svg",
    data: content,
    image: "logo.png",
    dotsOptions: {
      color: "#4700e5",
      type: "dots"
    },
    cornersSquareOptions: {
      type: "extra-rounded"
    },
    cornersDotOptions: {
      type: "dot"
    },
    backgroundOptions: {
      color: "#ffffff",
    },
    imageOptions: {
      crossOrigin: "anonymous",
    }
  });
  return await qrCode.toBuffer('png');
}