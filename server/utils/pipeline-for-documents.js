module.exports = [{
  "$group": {
    "_id": "$documentId",
    "disputes": {"$push": "$$ROOT"}
  }
},
  {
    "$lookup": {
      "from": "alerts",
      "localField": "disputes.alertId",
      "foreignField": "_id",
      "as": "alertObjects"
    }
  },
  {
    "$unwind": "$alertObjects"
  },
  {
    "$lookup": {
      "from": "scanhistories",
      "localField": "alertObjects.scanId",
      "foreignField": "_id",
      "as": "alertObjects.scanId"
    }
  },
  {
    "$unwind": {
      "path": "$alertObjects.scanId",
      "preserveNullAndEmptyArrays": true
    }
  },
  {
    "$lookup": {
      "from": "contents",
      "localField": "_id",
      "foreignField": "_id",
      "as": "documentObject"
    }
  },
  {
    "$unwind": {
      "path": "$documentObject",
      "preserveNullAndEmptyArrays": true
    }
  },
  {
    "$unwind": {
      "path": "$disputes",
      "preserveNullAndEmptyArrays": true
    }
  },
  {
    "$lookup": {
      "from": "alerts",
      "localField": "disputes.alertId",
      "foreignField": "_id",
      "as": "disputes.alertObject"
    }
  },
  {
    "$unwind": {
      "path": "$disputes.alertObject",
      "preserveNullAndEmptyArrays": true
    }
  },
  {
    "$group": {
      "_id": "$_id",
      "documentObject": {"$first": "$documentObject"},
      "disputes": {"$push": "$disputes"},
      "alertObjects": {"$first": "$alertObjects"}
    }
  },
  {
    "$project": {
      "documentId": "$documentObject",
      "disputes": {
        "$map": {
          "input": "$disputes",
          "as": "dispute",
          "in": {
            "$mergeObjects": [
              "$$dispute",
              {
                "alertId": {
                  "$cond": {
                    "if": {"$eq": ["$$dispute.alertId", "$alertObjects._id"]},
                    "then": "$alertObjects",
                    "else": {}
                  }
                }
              }
            ]
          }
        }
      },
      "_id": 0
    }
  }
];