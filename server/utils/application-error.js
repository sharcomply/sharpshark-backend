class ApplicationError extends Error {
  static NotUnique = () => new ApplicationError(1, 409, 'Element is not unique');
  static NotFound = () => new ApplicationError(2, 404, 'Element not found');
  static NotCreated = () => new ApplicationError(3, 400, 'Element not created');
  static NoAuthHeader = () => new ApplicationError(4, 403, 'Authorization header not specified');
  static BadUserToken = () => new ApplicationError(5, 401, 'Bad token');
  static InvalidCredentials = () => new ApplicationError(6, 401, 'Invalid credentials');
  static RequiredAttributes = () => new ApplicationError(7, 400, 'Required attributes not specified');
  static JsonValidation = (message) => new ApplicationError(8, 422, `JSON is bad: ${message}`);
  static AlreadyExists = () => new ApplicationError(9, 409, 'Already exists');
  static NoPermission = () => new ApplicationError(10, 403, 'No permission to perform action with that access level');
  static MailingError = () => new ApplicationError(11, 400, 'Send mail error');
  static NewPasswordError = () => new ApplicationError(12, 400, 'New password must be specified');
  static VerificationAlreadyExists = () => new ApplicationError(13, 400, 'You can\'t start new verification if you verified or verification already in progress');
  static VerificationNoDocuments = () => new ApplicationError(14, 400, 'You can\'t start verification without any documents uploaded');
  static AdminTokenNotFound = () => new ApplicationError(15, 401, 'Admin token not found');
  static WithdrawTimeError = () => new ApplicationError(16, 409, 'Your time to withdraw has not come');
  static WithdrawBalanceError = () => new ApplicationError(17, 409, 'You have insufficient balance to withdraw');
  static WithdrawMaximumError = () => new ApplicationError(18, 409, 'Withdraw maximum for account status reached');
  static ServiceError = () => new ApplicationError(19, 500, 'Internal service malfunction');
  static BadRequest = () => new ApplicationError(20, 400, `Required params are missing or malformed`);
  static JsonValidation = (message) => new ApplicationError(21, 422, `${message}`);
  static NotYouDocument = () => new ApplicationError(22, 400, `You cannot scan, this document does not belong to you`);
  static TooLowAggregatedScore = (message) => new ApplicationError(23, 400, `Scan aggregated score must be lower or equal 25, but ${message} provided`);
  static ActiveMonitoringsExists = () => new ApplicationError(24, 409, `Content already set for monitoring`);
  static ParentNotFoundError = () => new ApplicationError(25, 404, `Parent not found`);
  static TokenIsNotOnSale = () => new ApplicationError(26, 409, `Token is not on sale`);
  static ScansTotalError = () => new ApplicationError(27, 400, `Scans total must be at least 1`);
  static ContentLengthTooLowError = () => new ApplicationError(28, 400, `Content length must be at least 50`);
  static DisputeStatusError = () => new ApplicationError(29, 400, `Not able to send emails with that dispute status`);
  static DisputeEmailsError = () => new ApplicationError(30, 400, `Dispute emails not set`);
  static MaxProtectionsError = () => new ApplicationError(31, 409, `Servers are busy, please retry your request in a minute`);
  static MailFindError = () => new ApplicationError(32, 409, `Error mailing finding, please contact admin`);
  static PriceEstimatingError = () => new ApplicationError(33, 409, `Error estimating price, please contact admin`);
  static NftStorageUploadError = () => new ApplicationError(34, 409, `Error uploading to NFT storage, please try again later or check your text to special symbols`);

  constructor(code, httpStatusCode, message) {
    super();
    this.code = code;
    this.httpStatusCode = httpStatusCode;
    this.message = message;
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
  }
}

module.exports = ApplicationError;
