const uploadToS3 = require('./upload-to-s3');
const QRCode = require('qrcode');
const generateLogoQr = require('./generate-logo-qr');

module.exports = async (type_, content, userId, draftId, network, withLogo) => {
  const filename = `${userId}/${draftId}_${network}_${type_}.png`;
  let buffer;
  if (withLogo) {
    buffer = await generateLogoQr(content);
  } else {
    buffer = await QRCode.toBuffer(content, {
      type: 'png',
      errorCorrectionLevel: 'L',
    });
  }
  return uploadToS3(buffer, filename);
}