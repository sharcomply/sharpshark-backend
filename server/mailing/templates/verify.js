module.exports = {
    subject: "Sharp Shark: Email verify",
    html: `Dear user,<br><br>

Click to verify your email: <a href="<!link>"><!link></a><br><br>

Thanks,<br><br>

Sharp Shark`,
    text: `Dear user,

Click to verify your email: <!link>

Thanks,

Sharp Shark`
};
