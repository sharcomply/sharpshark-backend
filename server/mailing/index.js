const nodemailer = require("nodemailer");
const ApplicationError = require('../utils/application-error');
const templateBuilder = require('../mailing/template-builder');


let transporter = nodemailer.createTransport({
  host: process.env.MAILING_HOST,
  port: Number.parseInt(process.env.MAILING_PORT),
  pool: true,
  secure: process.env.MAILING_SECURE === 'true', // true for 465, false for other ports
  auth: {
    user: process.env.MAILING_USERNAME,
    pass: process.env.MAILING_PASSWORD
  }
});


module.exports = async (emailObj, ccEmails) => {
  const emailInfo = await templateBuilder(emailObj);
  const transporterObject = {
    from: '"SharpShark" <' + process.env.MAILING_SUPPORT_EMAIL + '>', // sender address
    to: emailInfo.email, // list of receivers
    subject: emailInfo.subject, // Subject line
    text: emailInfo.text, // plain text body,
    replyTo: emailInfo.replyTo || process.env.MAILING_SUPPORT_EMAIL,
    html: emailInfo.html // html body
  };
  if (ccEmails && ccEmails.length > 0) {
    transporterObject.cc = ccEmails;
  }
  try {
    await transporter.sendMail(transporterObject);
  } catch (err) {
    console.log(err);
    // throw ApplicationError.MailingError();
  }
};
