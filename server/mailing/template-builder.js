module.exports = async (obj) => {
  if (!obj.subject || !obj.text || !obj.email) {
    throw new Error('Subject, text and email must be specified');
  }
  const [text, html] = [obj.text, obj.html].map((item) => {
    if (!item) {
      return undefined;
    }
    return item
      .replace(/<!link>/g, obj.link)
  });
  return {
    subject: obj.subject,
    email: obj.email,
    replyTo: obj.replyTo,
    text: text,
    html: html
  };
};
