let connectedSockets = require('./connected-sockets');
const connectionSubscribers = require('./connection-subscribers');
let pinger = function(socket) {
  socket.send(JSON.stringify({action: "ping", uniqueId: socket.uniqueId}));
};
const uuid = require('uuid');

module.exports = (wss) => {
  //це конкретно сервер
  wss.on('connection', async (ws, req) => {
    ws.id = req.jwt.id;
    ws.uniqueId = uuid.v4();
    console.log(`User id ${ws.id} with unique=${ws.uniqueId} connected`)
    ws.pinger = setInterval(pinger, 30000, ws);
    connectedSockets[req.jwt.id] = ws;
    connectionSubscribers(ws);
  });

  wss.on('close', () => {
    console.info(`Websocket server was closed`)
  });

  wss.on('error', error => {
    console.error(`Websocket server fired error: ${error}`);
  })
};
