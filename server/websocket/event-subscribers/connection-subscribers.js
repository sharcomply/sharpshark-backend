const handle = require('../handlers');
let connectedSockets = require('./connected-sockets');

module.exports = (ws) => {
  //це конкретное подключение
  const errCallback = (error) => {
    //TODO: winston logger
    console.log("Error on ws message handler");
    console.log(error);
    ws.send(JSON.stringify({action: "error", message: error? error.message : "Unknown error"}));
  };

  ws.on('open', () => {

  });

  ws.on('message', data => {
    let message;
    try {
      message = JSON.parse(data);
    } catch (err) {
      //TODO: winston logger
      console.log(err);
      return ws.send(JSON.stringify({action: "error", message: "Can't parse message"}));
    }
    handle(ws, message, errCallback);
  });

  ws.on('error', error => {
    //TODO: winston logger
    console.error(`Websocket connection fired error: ${error}`)
  });

  ws.on('close', (code, reason) => {
    console.log(`Closed ${ws.uniqueId}, code: ${code}, reason: ${reason}`)
  })
};
