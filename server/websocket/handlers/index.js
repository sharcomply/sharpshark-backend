'use strict';

const getGlobbedFiles = require('../../utils/getGlobbedFiles');
const path = require('path');

const commandToHandlerMap = {};

getGlobbedFiles(path.join(__dirname, './commands/*'))
.forEach(fullPath => {
  let temp = fullPath.split('/').pop().split('.');
  temp.pop();
  let name = temp.join('.');
  commandToHandlerMap[name] = require(fullPath);
});

module.exports = (socket, message, callback) => {
  if (commandToHandlerMap[message.action]) {
    commandToHandlerMap[message.action](socket, message, callback);
  } else {
    return socket.send(JSON.stringify({action: "error", message: "Message can not be handled, as no handler exists for it"}))
  }
};