require('dotenv').config();
const sig = require('./server/blockchain/near/signature');
const BN = require('bn.js');

(async() => {
  const res = await sig.signForRequest('30698849147908961949323554861', '87122243397767492512932', 1664274346);
  console.log(res);
})();