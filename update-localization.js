require('dotenv').config();
const fs = require('fs');
const uploadToS3 = require('./server/utils/upload-to-s3');

const localizationPath = '/var/www/sharpshark-frontend/public/locales/en/translation.json';

(async () => {
  const file = fs.readFileSync(localizationPath);
  await uploadToS3(file, 'locales/base.json');
})();